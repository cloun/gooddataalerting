# README #


### What is this repository for? ###

GoodData Alerting application

### How do I get set up? ###

tar -czf  GoodDataSplunk.tar.gz GoodDataAlerting

### Source structure ###

./bin - pipeline scripts

./default - application configs

./default/data/ui - GUI html

./appserver/static - GUI JS

./appserver/static/vendor - thirdparty JS may be redownloaded from the official sites
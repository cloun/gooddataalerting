'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define(['underscore', "splunkjs/mvc/simpleform/input/submit"], function (_, SubmitButton) {
    return function () {
        /**
         *
         * @param {element} wrapper
         * @param {string} submitButtonText
         * @param {boolean} [showScheduleButton=false] If false, hide this button
         * @returns {{}}
         */
        function AssistantControlsFooter(wrapper, submitButtonText) {
            var _this = this;

            var showScheduleButton = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

            _classCallCheck(this, AssistantControlsFooter);

            this.controls = {};

            wrapper.addClass('mlts-assistant-controls-footer');

            this.controls.submitButton = $('<button>').attr('id', 'submitControl').addClass('btn btn-primary mlts-submit').text(submitButtonText).on('click', function (e) {
                $(e.target).trigger('submit', this);
            });

            if (showScheduleButton) {
                (function () {
                    _this.controls.scheduleButton = $('<button>').addClass('btn btn-primary mlts-schedule-fit').append($('<i>').addClass('icon icon-clock'));

                    _this.setSchedulingDisabled(false);

                    _this.submitButtonWrapper = $('<div>').addClass('btn-group');

                    wrapper.append(_this.submitButtonWrapper.append(_this.controls.submitButton, _this.controls.scheduleButton));

                    var wasOverScheduleButton = false;

                    var positionCheck = function positionCheck(e) {
                        var scheduleOffset = _this.controls.scheduleButton.offset();

                        if (e.pageX >= scheduleOffset.left && e.pageX <= scheduleOffset.left + _this.controls.scheduleButton.outerWidth()) {
                            if (!wasOverScheduleButton) {
                                _this.controls.scheduleButton.tooltip('show');
                                wasOverScheduleButton = true;
                            }
                        } else {
                            _this.controls.scheduleButton.tooltip('hide');
                            wasOverScheduleButton = false;
                        }
                    };

                    // since bootstrap tooltips don't work on disabled elements, attach events to the parent and trigger tooltips manually
                    _this.submitButtonWrapper.on('mousemove', _.throttle(positionCheck, 100, { trailing: false }));
                    _this.submitButtonWrapper.on('mouseleave', function (e) {
                        _this.controls.scheduleButton.tooltip('hide');
                        wasOverScheduleButton = false;
                    });
                })();
            } else {
                wrapper.append(this.controls.submitButton);
            }

            this.controls.openInSearchButton = $('<button>').addClass('btn btn-default mlts-open-in-search').text('Open in Search');
            this.controls.showSPLButton = $('<button>').addClass('btn btn-default mlts-show-spl').text('Show SPL');

            wrapper.append(this.controls.openInSearchButton, this.controls.showSPLButton);
        }

        _createClass(AssistantControlsFooter, [{
            key: 'getDisabled',
            value: function getDisabled() {
                return this.controls.submitButton.attr('disabled') === 'disabled';
            }
        }, {
            key: 'setDisabled',
            value: function setDisabled(disabled) {
                this.controls.submitButton.attr('disabled', disabled);
                this.controls.openInSearchButton.attr('disabled', disabled);
                this.controls.showSPLButton.attr('disabled', disabled);
                if (this.controls.scheduleButton != null) this.controls.scheduleButton.attr('disabled', disabled || this.schedulingDisabled);
            }
        }, {
            key: 'setSchedulingDisabled',
            value: function setSchedulingDisabled(disabled, disabledReason) {
                this.schedulingDisabled = disabled;

                if (this.controls.scheduleButton != null) {
                    // only re-enable the schedule button if the controls are already enabled

                    if (this.getDisabled() === false) {
                        this.controls.scheduleButton.attr('disabled', disabled);
                    }

                    this.controls.scheduleButton.tooltip('destroy');

                    if (disabled === false || disabledReason != null) {
                        var scheduleTooltipText = disabled ? disabledReason : this.controls.submitButton.text() + ' On A Schedule';

                        this.controls.scheduleButton.tooltip({
                            animation: false,
                            title: scheduleTooltipText,
                            container: 'body', // prevents breakage of rounded corners on button
                            trigger: 'focus' // for this to work on disabled elements, we need to have to put the listeners on the wrapper element and manually trigger the tooltip
                        });
                    }
                }
            }
        }]);

        return AssistantControlsFooter;
    }();
});
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define([], function () {
    return function () {
        /**
         *
         * @param {element}         panel
         * @param {object|boolean} [footerButtons={}]                        If false, hide footer buttons; can also be an object to control individual buttons
         * @param {boolean}        [footerButtons.openInSearchButton=true]   If false, hide this button
         * @param {boolean}        [footerButtons.showSPLButton=true]        If false, hide this button
         * @param {boolean}        [footerButtons.scheduleAlertButton=false] If false, hide this button
         * @returns {{footer: (*|jQuery)}}
         */
        function AssistantPanelFooter(panel) {
            var footerButtons = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            _classCallCheck(this, AssistantPanelFooter);

            this.footer = $('<div>').addClass('mlts-panel-footer');

            panel.find('.panel-body').append(this.footer);

            if (footerButtons !== false) {
                if (footerButtons.openInSearchButton !== false) {
                    this.openInSearchButton = $('<button>').addClass('btn btn-default mlts-open-in-search').text('Open in Search');
                }

                if (footerButtons.showSPLButton !== false) {
                    this.showSPLButton = $('<button>').addClass('btn btn-default mlts-show-spl').text('Show SPL');
                }

                if (footerButtons.scheduleAlertButton === true) {
                    this.scheduleAlertButton = $('<button>').addClass('btn btn-default mlts-schedule-alert').text('Schedule Alert');
                }

                this.footer.append(this.openInSearchButton, this.showSPLButton, $('<span>').append(this.scheduleAlertButton));
            }
        }

        _createClass(AssistantPanelFooter, [{
            key: 'setSchedulingDisabled',
            value: function setSchedulingDisabled(disabled, disabledReason) {
                if (this.scheduleAlertButton != null) {
                    this.scheduleAlertButton.attr('disabled', disabled);

                    this.scheduleAlertButton.parent().tooltip('destroy');

                    if (disabled && disabledReason != null) {
                        this.scheduleAlertButton.parent().tooltip({
                            title: disabledReason
                        });
                    }
                }
            }
        }]);

        return AssistantPanelFooter;
    }();
});
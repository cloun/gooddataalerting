'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

define(['underscore', './Footer'], function (_, AssistantPanelFooter) {
    return function (_AssistantPanelFooter) {
        _inherits(AssistantPanel, _AssistantPanelFooter);

        /**
         *
         * @param {object}         [options]
         * @param {element}        options.el
         * @param {string}         options.title
         * @param {string|object}  [options.tooltip] Either a string or a Bootstrap tooltip options object
         * @param {object}         [options.viz] A built-in Splunk visualization or a ModViz
         * @param {object}         [options.vizOptions] Arguments to be passed to the visualization
         * @param {object|boolean} [options.panelWrapper] Either a jQuery reference to the panel body, or "false" to use the panel itself
         * @param {object|boolean} [options.footerButtons]
         * @returns {object}
         */
        function AssistantPanel() {
            var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

            _classCallCheck(this, AssistantPanel);

            var assistantPanelOptions = _.extend({
                el: null,
                title: '',
                tooltip: null,
                viz: null,
                vizOptions: {},
                panelWrapper: null,
                footerButtons: {}
            }, options);

            var _this = _possibleConstructorReturn(this, (AssistantPanel.__proto__ || Object.getPrototypeOf(AssistantPanel)).call(this, assistantPanelOptions.el, assistantPanelOptions.footerButtons));

            _this.header = $('<h3>').addClass('mlts-panel-header');
            _this.title = $('<a>').addClass('mlts-panel-title').html(assistantPanelOptions.title);
            _this.body = $('<div>').addClass('mlts-panel-body');

            var panelWrapper = assistantPanelOptions.panelWrapper === false ? assistantPanelOptions.el : assistantPanelOptions.panelWrapper || assistantPanelOptions.el.find('.panel-body');

            panelWrapper.prepend(_this.header.append(_this.title), _this.body);

            if (assistantPanelOptions.tooltip != null) {
                _this.title.tooltip(typeof assistantPanelOptions.tooltip === 'string' ? { title: assistantPanelOptions.tooltip } : assistantPanelOptions.tooltip);
            }

            if (assistantPanelOptions.viz != null) {
                if (assistantPanelOptions.vizOptions.height == null) {
                    var panelHeight = _this.body.height();
                    if (panelHeight > 0) assistantPanelOptions.vizOptions.height = panelHeight;
                }

                _this.viz = new assistantPanelOptions.viz(_.extend({ el: _this.body }, assistantPanelOptions.vizOptions)).render();
            }
            return _this;
        }

        return AssistantPanel;
    }(AssistantPanelFooter);
});
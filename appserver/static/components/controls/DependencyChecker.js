'use strict';

define(['splunkjs/mvc', 'components/controls/Messages', 'components/data/parameters/LooseVersion'], function (mvc, Messages, LooseVersion) {
    var service = mvc.createService({ owner: 'nobody' });

    var minSplunkVersion = '6.4.0';

    var dependencyStatusMessages = {
        splunkVersion: {
            message: 'You must have Splunk version ' + minSplunkVersion + ' or later installed to use the ' +
            'GoodData alerting app.',
            additionalContent: null
        }
    };

    function handleDependencyStatus(dependencyStatus, callback) {
        var hasDependencies = true;

        if (dependencyStatus != null) {
            for (var i = 0; i < dependencyStatus.length; i++) {
                if (dependencyStatus[i].status === false) {
                    hasDependencies = false;

                    var body = $(".dashboard-body");
                    body.hide();

                    var dependencyWrapper = $('<div>').addClass('dependency-checker-wrapper');
                    dependencyWrapper.insertBefore(body);

                    var errorWrapper = $('<div>').addClass("dependency-checker-error");
                    dependencyWrapper.append(errorWrapper);

                    var dependencyMessage = dependencyStatusMessages[dependencyStatus[i].type];

                    Messages.setAlert(errorWrapper, dependencyMessage.message, null, 'alert-inline');

                    if (dependencyMessage.additionalContent != null) {
                        dependencyWrapper.append(dependencyMessage.additionalContent);
                    }

                    break; // we only want to display the first error
                }
            }
        }

        callback(hasDependencies);
    }

    return {
        check: function check(callback) {
            service.request('/apps/local', 'GET', { count: 0 }, null, null, { 'Content-Type': 'application/json' }, function (error, response) {
                if (error == null) {
                    // this is an array to preserve the order dependency status errors show up in
                    var dependencyStatuses = [{
                        type: 'splunkVersion',
                        // response.data.generator.version is the Splunk version
                        status: LooseVersion.compareVersions(response.data.generator.version, minSplunkVersion) >= 0
                    }];

                    handleDependencyStatus(dependencyStatuses, callback);
                } else {
                    // on failure, bail out on the dependency check and let errors surface elsewhere
                    handleDependencyStatus(null, callback);
                }
            });
        }
    };
});
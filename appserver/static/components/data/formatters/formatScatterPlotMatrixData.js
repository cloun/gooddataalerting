"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

define(["components/data/parameters/ColorPalette"], function (ColorPalette) {
    /**
     * formatting data for scatter plot matrix
     */
    return function (data) {
        var maxSeriesToTrim = arguments.length <= 1 || arguments[1] === undefined ? 50 : arguments[1];
        var plotPointThreshold = arguments.length <= 2 || arguments[2] === undefined ? 1000 : arguments[2];
        var maxFields = arguments.length <= 3 || arguments[3] === undefined ? 6 : arguments[3];

        var isDataRowsTrimmed = false,
            isFieldsTrimmed = false,
            isSeriesTrimmed = false;
        var series = [];
        var count = 0;

        /**
         * Data truncation step:
         * 1. keep the first maxFields fields for all data.
         * 2. keep the first 1000 rows.
         * 3. group all data by its series (the first field), keep the first X series.
         */
        data.fields = data.fields.slice(0, maxFields).map(function (field) {
            //slice the first up to 6 fields
            return field.name;
        });
        if (data.rows[0] && data.fields.length < data.rows[0].length) isFieldsTrimmed = true;

        if (data.rows.length > plotPointThreshold) {
            data.rows = data.rows.slice(0, plotPointThreshold);
            isDataRowsTrimmed = true;
        }

        var newDataRow = [];
        var isAllNumeric = data.rows.every(function (row) {
            if (!row.every(function (ele) {
                return ele == null;
            })) {
                // remove rows with all null values
                var numericFields = row.slice(1);
                if (numericFields.some(function (ele) {
                    return isNaN(ele);
                })) {
                    return false; // break the loop if non-numeric value detected
                } else {
                    var _ret = function () {
                        /**
                         *  convert value to numeric from the second field
                         */
                        numericFields = numericFields.map(function (ele) {
                            var numericValue = parseFloat(ele);
                            if (!isNaN(numericValue)) {
                                // parse the number when it's not null or ' '
                                return numericValue;
                            } else {
                                return null; // set values like ' ' and undefined to null
                            }
                        });
                        var newRow = [row[0]].concat(numericFields);

                        /**
                         * group by series
                         */
                        var label = newRow[0];
                        var hasLabelexist = series.some(function (serie) {
                            return serie.name === label;
                        });
                        if (!hasLabelexist) {
                            if (series.length < maxSeriesToTrim) {
                                //add to newDataRow array only if it belongs to the first 20 series.
                                series.push({ name: label, color: ColorPalette.getColorByIndex(count++), visible: true });
                                newDataRow.push(newRow);
                            } else {
                                // otherwise don't add the point to returned data.
                                isSeriesTrimmed = true;
                            }
                        } else {
                            newDataRow.push(newRow);
                        }
                        return {
                            v: true
                        };
                    }();

                    if ((typeof _ret === "undefined" ? "undefined" : _typeof(_ret)) === "object") return _ret.v;
                }
            }
            return true;
        });
        data.rows = newDataRow;

        if (!isAllNumeric) {
            return false;
        } else {
            return {
                data: data,
                options: {
                    seriesArray: series,
                    isSeriesTrimmed: isSeriesTrimmed,
                    isFieldsTrimmed: isFieldsTrimmed,
                    isDataRowsTrimmed: isDataRowsTrimmed
                }
            };
        }
    };
});
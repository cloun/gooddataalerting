'use strict';

define([], function () {
    /**
     * @param [string[]] algorithmParameters;
     */
    return function parseAlgorithmParameters(algorithmParameters) {
        var parsedParameters = {};

        algorithmParameters.forEach(function (param) {
            if (typeof param === 'string') {
                var splitLocation = param.indexOf('=');

                if (splitLocation > 0) {
                    var paramName = param.substring(0, splitLocation);
                    parsedParameters[paramName] = param.substring(splitLocation + 1);
                }
            }
        });

        return parsedParameters;
    };
});
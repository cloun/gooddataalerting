'use strict';

/**
 * A class for keeping track of the validity of form controls.
 */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define([], function () {
    return function () {
        function ControlValidityStore() {
            _classCallCheck(this, ControlValidityStore);

            this.store = {};
        }

        /**
         *
         * @param {string}  id
         * @param {boolean} isValid
         */


        _createClass(ControlValidityStore, [{
            key: 'set',
            value: function set(id, isValid) {
                this.store[id] = isValid;
            }

            /**
             * Checks whether all entries in controlValidityStore are true
             * @returns {boolean}
             */

        }, {
            key: 'getAll',
            value: function getAll() {
                var _this = this;

                return Object.keys(this.store).every(function (validity) {
                    return _this.store[validity];
                });
            }
        }, {
            key: 'clearAll',
            value: function clearAll() {
                var _this2 = this;

                Object.keys(this.store).forEach(function (key) {
                    delete _this2.store[key];
                });
            }
        }]);

        return ControlValidityStore;
    }();
});
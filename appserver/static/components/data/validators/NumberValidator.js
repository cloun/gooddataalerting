'use strict';

define([], function () {
    return {
        /**
         * @param {string|number} value
         * @param {object}        [options]
         * @param {number}        [options.min]
         * @param {number}        [options.max]
         * @param {boolean}       [options.minExclusive=false]
         * @param {boolean}       [options.maxExclusive=false]
         * @param {boolean}       [options.allowFloats = true]
         */
        validate: function validate(value) {
            var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            var parsedValue = null;
            var isNumber = false;

            if (typeof value === 'number') {
                parsedValue = value;

                // if floats allowed or number is not a float
                isNumber = options.allowFloats !== false || value % 1 === 0;
            } else {
                parsedValue = options.allowFloats !== false ? parseFloat(value) : parseInt(value, 10);

                // the second half of the check makes sure that floats-as-strings with a leading "." (such as ".5") pass validation
                isNumber = parsedValue.toString() === value || typeof value === 'string' && options.allowFloats !== false && value[0] === '.' && parsedValue.toString() === '0' + value;
            }

            var minValid = options.min == null || (options.minExclusive ? parsedValue > options.min : parsedValue >= options.min);
            var maxValid = options.max == null || (options.maxExclusive ? parsedValue < options.max : parsedValue <= options.max);

            return isNumber && minValid && maxValid;
        }
    };
});
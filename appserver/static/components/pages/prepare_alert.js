// 'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _templateObject = _taggedTemplateLiteral(["| $excludeToken$ `modvizgdpredict($predictFieldsToken|s$, $predictAlgorithmToken|s$, $holdbackToken|s$,\n" +
    "                                             $holdbackToken|s$, $periodValueToken|s$, $confidenceIntervalToken|s$, $nonnegativeValueToken$, \"\")`"],
    [" $excludeToken$ \\`modvizgdpredict($predictFieldsToken|s$, $predictAlgorithmToken|s$, $holdbackToken|s$,\n" +
    "                                             $holdbackToken|s$, $periodValueToken|s$, $confidenceIntervalToken|s$, $nonnegativeValueToken$, \"\")\\`"]),
    _templateObjectAlert = _taggedTemplateLiteral(["| search earliest=-$windowToken$\n | $excludeToken$ `modvizgdpredict($metricToken$, $predictAlgorithmToken|s$, $holdbackToken|s$,\n" +
        "                                             $holdbackToken|s$, $periodValueToken|s$, $confidenceIntervalToken|s$, $nonnegativeValueToken$, \"$byClause$\")` \n"],
        ["search earliest=-$windowToken$\n | $excludeToken$ \\`modvizgdpredict($metricToken$, $predictAlgorithmToken|s$, $holdbackToken|s$,\n" +
        "                                             $holdbackToken|s$, $periodValueToken|s$, $confidenceIntervalToken|s$, $nonnegativeValueToken$, \"$byClause$\")\\` \n"]),

    _templateObject2 = _taggedTemplateLiteral(["| loadjob $searchBarSearchJobIdToken$\n                                   | head 1\n" +
    "                                   | transpose\n                                   | fields column \n" +
    "                                   | search column != \"column\" AND column != \"_*\""], ["| loadjob $searchBarSearchJobIdToken$\n" +
    "                                   | head 1\n                                   | transpose\n                                   | fields column \n" +
    "                                   | search column != \"column\" AND column != \"_*\""]),
    _templateObject4 = _taggedTemplateLiteral(["| addinfo | eval training_split = if(info_max_time!=\"+Infinity\", if(\"$trainingSplitToken$\"!=\"-Infinity\", relative_time(info_max_time, \"-$trainingSplitToken$\"), 0), 0) " +
        "| eval isOutlier = if(_time> training_split AND prediction!=\"\" AND outliersNum>0, 1, 0)"],
        ["| addinfo | eval training_split = if(info_max_time!=\"+Infinity\", if(\"$trainingSplitToken$\"!=\"-Infinity\", relative_time(info_max_time, \"-$trainingSplitToken$\"), 0), 0)      " +
        "| eval isOutlier = if(_time> training_split AND prediction!=\"\" AND outliersNum>0, 1, 0)"]
    ),
    _templateObject5 = _taggedTemplateLiteral(
        ["| inputlookup ", "_lookup\n                                   | eval \"Search query\"=search_query,\n" +
    "   \"Field to predict\"=field_to_predict, Method=method, Withhold=withhold,\n " +
    "    \"Forecast next k values\"=future_values,\"Confidence Interval\"=confidence_interval,\n " +
    "  \"Period\"=period,\"Timespan\"=span,\"Nonnegative\"=nonnegative,\"GDExclude\"=exclude,\"R² Statistic\"=r_squared, \"RMSE\"=rmse, \"# of outliers\"=outliers_count,\n" +
    "  \"Actions\"=actions"],
        ["| inputlookup ", "_lookup\n    " +
    "   | eval \"Search query\"=search_query,\n " +
    "  \"Field to predict\"=field_to_predict, Method=method, Withhold=withhold,\n " +
    "  \"Forecast next k values\"=future_values,\"Confidence Interval\"=confidence_interval,\n " +
    "  \"Period\"=period,\"Timespan\"=span,\"Nonnegative\"=nonnegative,\"GDExclude\"=exclude,\"R² Statistic\"=r_squared, \"RMSE\"=rmse, \"# of outliers\"=outliers_count,\n " +
    "  \"Actions\"=actions"]);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

require(["jquery", "underscore", "splunkjs/mvc/dropdownview", "splunkjs/mvc/singleview", "splunkjs/mvc/textinputview",
    "splunkjs/mvc/checkboxview", "splunkjs/mvc/tableview", "splunkjs/mvc/visualizationregistry",
    "components/splunk/Searches", "components/splunk/Forms", "components/data/parameters/ParseSearchParameters",
    "components/data/formatters/getFieldFromData", "components/data/serializers/ShowcaseHistorySerializer",
    "components/controls/DrilldownLinker", "components/controls/Messages", 'components/splunk/AlertModal',
    'components/splunk/KVStore', 'components/splunk/SearchBarWrapper', "Options",
    "components/controls/AssistantControlsFooter", "components/controls/AssistantPanel/Master",
    "components/controls/AssistantPanel/Footer", "components/controls/Modal", "components/controls/QueryHistoryTable",
    "components/controls/SearchStringDisplay", "components/controls/Spinners", "components/controls/Tabs",
    "components/data/formatters/compactTemplateString",
    "splunkjs/mvc/timerangeview"], function (
        $, _, DropdownView, SingleView, TextInputView,
        CheckboxView, TableView, VisualizationRegistry, Searches, Forms, ParseSearchParameters, getFieldFromData,
        ShowcaseHistorySerializer, DrilldownLinker, Messages, AlertModal, KVStore, SearchBarWrapper, Options,
        AssistantControlsFooter, AssistantPanel, AssistantPanelFooter, Modal, QueryHistoryTable, SearchStringDisplay,
        Spinners, Tabs, compact, TimeRangeView) {

    var pageName = 'forecast';
    var searchManagerId = 'predictSearch';
    var appName = Options.getOptionByName('appName');
    var smallLoaderScale = Options.getOptionByName('smallLoaderScale');

    var historyCollectionId = pageName + "_history";
    var exclusionsCollectionId = pageName + "_exclusions";

    var predictSearchString = compact(_templateObject);

    var modvizpredictExplain = 'forecast using a macro that formats the `gdpredict` command output for a different visualization';

    var submitButtonText = 'Forecast';

    var isRunning = false;
    var currentSampleSearch = null;
    var baseSearchString = null;
    var baseTimerange = null;

    var $exclusionsPanel = function() {

        $exclusionsPanel = $('#exclusionsPanel');

        var mytimerange_custom = null;

        // Create a custom time range picker
        // Show the Presets panel, hide the Real-time and Advanced panels
        var mypresetsettings = {
            showPresets: true,
            showCustomRealTime: false,
            showCustomAdvanced: false,
            showPresetsAllTime: true
        };
        // Define custom preset values

        var tomorrow = new Date();
        tomorrow.setDate((new Date()).getDate()+1);
        tomorrow.setHours(0);
        tomorrow.setMinutes(0);
        tomorrow.setSeconds(0);
        var mypresetvalues = [
            {
                label: 'Tomorrow',
                earliest_time: (tomorrow.getTime()/1000).toString(),
                latest_time: ((tomorrow.getTime()+24*3600)/1000).toString()}
        ];
        // Instantiate a view using the custom time range picker
        mytimerange_custom = new TimeRangeView({
            id: "mytimerangeview",
            dialogOptions: mypresetsettings,
            preset: 'Tomorrow',
            presets: mypresetvalues,
            el: $("#exclusionRangePicker")
        }).render();


        function draw_exclusions() {

            var $table_body = $('#exclusions-table tbody');

            KVStore.getCollection(exclusionsCollectionId, function (err, response) {
                $table_body.empty();
                $.each(response.data, function(i, kv_item) {
                    var $tr = $('<tr class="shared-resultstable-resultstablerow" ' +
                        'data-view="views/shared/results_table/ResultsTableRow">');
                    if (i%2==0) {
                        $tr.addClass('odd');
                    }
                    var $td_ts = $('<td>');
                    $td_ts.html(
                        new Date(kv_item.range_from*1000) + '&nbsp;&mdash;&nbsp;' + new Date(kv_item.range_to*1000));
                    $tr.append($td_ts);
                    var $td_hosts = $('<td>');
                    if (Array.isArray(kv_item.hosts) && kv_item.hosts.length > 0) {
                        var $ul = $('<ul>');
                        $.each(kv_item.hosts, function (j, h) {
                            var $li = $('<li>');
                            $li.html(h);
                            $ul.append($li);
                        });
                        $td_hosts.append($ul);
                    }
                    else {
                        $td_hosts.append($('<span>ALL</span>'));
                    }
                    $tr.append($td_hosts);
                    var $td_comment = $('<td>');
                    $td_comment.html(kv_item.comment);
                    $tr.append($td_comment);
                    var $td_del = $('<td>');
                    var $del_btn = $('<button class="btn btn-default" data-exclusion-remove-id=' + kv_item.time + '>');
                    $del_btn.html('Remove from exclusions');
                    $del_btn.click(function () {
                        KVStore.deleteCollection(exclusionsCollectionId, kv_item._key, function(еrr) {
                            if (err==null) {
                                $tr.remove();
                            }
                        });
                    });
                    $td_del.append($del_btn);
                    $tr.append($td_del);
                    $table_body.append($tr);
                });
            });

        }

        var $exclude_comment = $('#comment-exclude');
        var $hosts = $('#hosts-exclude');
        var $exclude_sbm_btn = $('#submitExclusion');
        $exclude_sbm_btn.click(function() {
            var hosts = [];
            $.each($('#hosts-exclude').val().split(','), function (i, h) {
                var t = h.trim();
                if (t) {
                    hosts.push(t)
                }
            });
            var item = {
                time: new Date().getTime(),
                hosts: hosts,
                comment: $exclude_comment.val(),
                range_from: mytimerange_custom.val().earliest_time,
                range_to: mytimerange_custom.val().latest_time
            };
            KVStore.setCollection(
                exclusionsCollectionId,
                item,
                function (err) {
                    if(err==null) {
                        draw_exclusions();
                        $exclude_comment.val('');
                        $hosts.val('');
                        $exclude_sbm_btn.prop( "disabled", true);
                    }
                }
            );
        });

        $exclude_comment.on('input', function () {
            if ($exclude_comment.val() && $exclude_comment.val().length > 5) {
                $exclude_sbm_btn.prop( "disabled", false);
            }
        });
        draw_exclusions();

        return $exclusionsPanel;
    }();

    var controlValidity = function () {
        var controlValidityStore = {};

        return {
            set: function set(id, value) {
                controlValidityStore[id] = value;
                $(document).trigger('generalValidity', this.getAll());
            },
            /**
             * Checks whether all entries in controlValidityStore are true
             * @returns {boolean}
             */
            getAll: function getAll() {
                return Object.keys(controlValidityStore).every(function (validity) {
                    return controlValidityStore[validity];
                });
            }
        };
    }();

    var historySerializer = new ShowcaseHistorySerializer(historyCollectionId, {
        _time: null,
        search_query: null,
        earliest_time: null,
        latest_time: null,
        field_to_predict: null,
        method: null,
        withhold: null,
        by_clause: null,
        exclude: null,
        nonnegative: null,
        metric:null,
        learning: null,
        span:null,
        confidence_interval: null,
        period: null,
        r_squared: null,
        rmse: null,
        outliers_count: null
    }, function () {
        Searches.getSearchManager('queryHistorySearch').startSearch();
    });

    function setupSearches() {
        (function setupSearchBarSearch() {
            Searches.setSearch("searchBarSearch", {
                targetJobIdTokenName: "searchBarSearchJobIdToken",
                onStartCallback: function onStartCallback() {
                    hideErrorMessage();
                    hidePanels();
                },
                onDoneCallback: function onDoneCallback(searchManager) {
                    DrilldownLinker.setSearchDrilldown(datasetPreviewTable.$el.prev('h3'), searchManager.search);
                },
                onErrorCallback: function onErrorCallback(errorMessage) {
                    showErrorMessage(errorMessage);
                    hidePanels();
                }
            });
        })();

        (function setupPredictionFieldsSearch() {
            Searches.setSearch("predictionFieldsSearch", {
                searchString: compact(_templateObject2),
                onStartCallback: function onStartCallback() {
                    hideErrorMessage();
                },
                onErrorCallback: function onErrorCallback(errorMessage) {
                    showErrorMessage(errorMessage);
                    hidePanels();
                }
            });
        })();

        (function setupPredictSearch() {
            var vizQueryArray = [];
            var vizQuerySearch = null;

            var vizOptions = DrilldownLinker.parseVizOptions({
                category: 'custom',
                type: appName + ".ForecastViz"
            });

            function openInSearch() {
                var _getVizQuery = getVizQuery();

                var _getVizQuery2 = _slicedToArray(_getVizQuery, 2);

                vizQueryArray = _getVizQuery2[0];
                vizQuerySearch = _getVizQuery2[1];

                window.open(DrilldownLinker.getUrl('search', vizQuerySearch, vizOptions), '_blank');
            }

            function showSPL(e) {
                // adjust the modal title depending on whether or not the modal is from the plot or not
                var modalTitle = forecastPanel.showSPLButton.first().is($(e.target)) ? 'Plot forecast' : 'Calculate the forecast';

                var _getVizQuery3 = getVizQuery();

                var _getVizQuery4 = _slicedToArray(_getVizQuery3, 2);

                vizQueryArray = _getVizQuery4[0];
                vizQuerySearch = _getVizQuery4[1];

                SearchStringDisplay.showSearchStringModal(searchManagerId + "Modal", modalTitle, vizQueryArray, [null, [modvizpredictExplain]], baseTimerange, vizOptions);
            }

            assistantControlsFooter.controls.openInSearchButton.on('click', openInSearch);
            assistantControlsFooter.controls.showSPLButton.on('click', showSPL);

            forecastPanel.openInSearchButton.on('click', openInSearch);
            forecastPanel.showSPLButton.on('click', showSPL);

            Searches.setSearch(searchManagerId, {
                autostart: false,
                targetJobIdTokenName: "predictJobIdToken",
                searchString: "| loadjob $searchBarSearchJobIdToken$ " + predictSearchString,
                onStartCallback: function onStartCallback() {
                    Spinners.showLoadingOverlay(forecastPanel.viz.$el);

                    hideErrorMessage();
                    updateForm(true);

                    currentSampleSearch = null;

                    var searchManager = Searches.getSearchManager(searchManagerId);
                    var jobId = Searches.getSid(searchManager);

                    var searchAttributes = Searches.getSearchManager('searchBarSearch').search.attributes;

                    var collection = {
                        _time: parseInt(new Date().valueOf() / 1000, 10),
                        search_query: searchAttributes.search,
                        earliest_time: searchAttributes.earliest_time,
                        latest_time: searchAttributes.latest_time,
                        field_to_predict: predictFieldsControl.val(),
                        method: predictAlgorithmControl.val(),
                        withhold: holdbackControl.val(),
                        by_clause: byClauseControl.val(),
                        exclude: excludeControl.val(),
                        confidence_interval: confidenceIntervalControl.val(),
                        period: periodValueControl.val() || 0,
                        span: timespanControl.val(),
                        nonnegative: nonnegativeCheckboxControl.val(),
                        metric:metricControl.val(),
                        learning: windowControl.val()
                    };
                    historySerializer.persist(jobId, collection);

                    var _getVizQuery5 = getVizQuery();

                    var _getVizQuery6 = _slicedToArray(_getVizQuery5, 2);

                    vizQueryArray = _getVizQuery6[0];
                    vizQuerySearch = _getVizQuery6[1];

                    DrilldownLinker.setSearchDrilldown(forecastPanel.title, vizQuerySearch, vizOptions);
                },
                onDoneCallback: function onDoneCallback() {
                    showPanels();

                    Searches.getSearchManager("predictionOutliersSearch").startSearch();
                    Searches.getSearchManager("regressionstatisticsSearch").startSearch();
                },
                onErrorCallback: function onErrorCallback(errorMessage) {
                    showErrorMessage(errorMessage);
                    hidePanels();
                },
                onFinallyCallback: function onFinallyCallback() {
                    updateForm(false);
                    Spinners.hideLoadingOverlay(forecastPanel.viz.$el);
                }
            });
        })();

        (function setupPredictionOutliersSearch() {
            var identifyOutliersSearchQueryArray = [compact(_templateObject4)];


            var findOutliersSearchQueryArray = identifyOutliersSearchQueryArray.concat(['| where isOutlier=1', '| fields - isOutlier']);

            var outlierExplain = 'find the outliers among events that have both an actual and a predicted value';

            var vizQueryArray = [];
            var vizQuerySearch = null;

            var outliersOverTimeVizOptions = DrilldownLinker.parseVizOptions({
                category: 'custom',
                type: appName + ".LinesViz"
            });

            predictionOutliersStatisticPanel.openInSearchButton.on('click', function () {
                window.open(DrilldownLinker.getUrl('search', vizQuerySearch), '_blank');
            });

            predictionOutliersStatisticPanel.showSPLButton.on('click', function () {
                SearchStringDisplay.showSearchStringModal('predictionOutliersSearchStringDisplayModal', 'Show forecast outliers', vizQueryArray, [null, modvizpredictExplain, outlierExplain], baseTimerange);
            });

            predictionOutliersStatisticPanel.plotOutliersOverTimeButton.on('click', function () {
                var _getVizQuery7 = getVizQuery([predictSearchString].concat(identifyOutliersSearchQueryArray));

                var _getVizQuery8 = _slicedToArray(_getVizQuery7, 2);

                vizQueryArray = _getVizQuery8[0];
                vizQuerySearch = _getVizQuery8[1];

                var outliersOverTimeSearch = DrilldownLinker.createSearch(vizQueryArray.concat(['| table _time, isOutlier']), baseTimerange);
                window.open(DrilldownLinker.getUrl('search', outliersOverTimeSearch, outliersOverTimeVizOptions), '_blank');
            });

            Searches.setSearch("predictionOutliersSearch", {
                autostart: false,
                searchString: ['| loadjob $predictJobIdToken$'].concat(findOutliersSearchQueryArray, ['| stats count']),
                onStartCallback: function onStartCallback() {
                    Spinners.showLoadingOverlay(predictionOutliersStatisticPanel.viz.$el, smallLoaderScale);

                    var _getVizQuery9 = getVizQuery([predictSearchString].concat(findOutliersSearchQueryArray));

                    var _getVizQuery10 = _slicedToArray(_getVizQuery9, 2);

                    vizQueryArray = _getVizQuery10[0];
                    vizQuerySearch = _getVizQuery10[1];


                    DrilldownLinker.setSearchDrilldown(predictionOutliersStatisticPanel.title, vizQuerySearch);
                },
                onDataCallback: function onDataCallback(data) {
                    var searchManager = Searches.getSearchManager(searchManagerId);
                    var jobId = Searches.getSid(searchManager);

                    var resultIndex = data.fields.indexOf('count');
                    var collection = {
                        outliers_count: data.rows[0][resultIndex]
                    };

                    historySerializer.persist(jobId, collection);
                },
                onFinallyCallback: function onFinallyCallback() {
                    Spinners.hideLoadingOverlay(predictionOutliersStatisticPanel.viz.$el);
                }
            });
        })();

        (function setupRegressionstatisticsSearch() {
            var regressionstatisticsSearchQueryArray = ["| addinfo | eval training_split = if(info_max_time!=\"+Infinity\", if(\"$trainingSplitToken$\"!=\"-Infinity\", relative_time(info_max_time, \"-$trainingSplitToken$\"), 0), 0)" +
            "| where _time>training_split AND prediction!=\"\" AND '$predictFieldsToken$' != \"\"", '| `regressionstatistics($predictFieldsToken|s$, prediction)`'];

            var vizQueryArray = [];
            var vizQuerySearch = null;

            regressionStatisticsPanel.openInSearchButton.on('click', function () {
                window.open(DrilldownLinker.getUrl('search', vizQuerySearch), '_blank');
            });

            regressionStatisticsPanel.showSPLButton.on('click', function () {
                SearchStringDisplay.showSearchStringModal('regressionstatisticsSearchStringDisplayModal', 'Compute R² and root mean squared error (RMSE)', vizQueryArray, [null, modvizpredictExplain, null, 'use the `regressionstatistics` macro to compute R² and RMSE'], baseTimerange);
            });

            Searches.setSearch("regressionstatisticsSearch", {
                autostart: false,
                searchString: ['| loadjob $predictJobIdToken$'].concat(regressionstatisticsSearchQueryArray),
                onStartCallback: function onStartCallback() {
                    Spinners.showLoadingOverlay(regressionStatisticsPanel.spinnerAnchor, smallLoaderScale);

                    var _getVizQuery11 = getVizQuery([predictSearchString].concat(regressionstatisticsSearchQueryArray));

                    var _getVizQuery12 = _slicedToArray(_getVizQuery11, 2);

                    vizQueryArray = _getVizQuery12[0];
                    vizQuerySearch = _getVizQuery12[1];


                    DrilldownLinker.setSearchDrilldown(r2StatisticPanel.title, vizQuerySearch);
                    DrilldownLinker.setSearchDrilldown(rootMeanSquaredErrorStatisticPanel.title, vizQuerySearch);
                },
                onDataCallback: function onDataCallback(data) {
                    var searchManager = Searches.getSearchManager(searchManagerId);
                    var jobId = Searches.getSid(searchManager);

                    var r2Index = data.fields.indexOf('rSquared');
                    var rmseIndex = data.fields.indexOf('RMSE');
                    var collection = {
                        rmse: data.rows[0][rmseIndex],
                        r_squared: data.rows[0][r2Index]
                    };

                    historySerializer.persist(jobId, collection);
                },
                onFinallyCallback: function onFinallyCallback() {
                    Spinners.hideLoadingOverlay(regressionStatisticsPanel.spinnerAnchor);
                }
            });
        })();
    }

    (function setupQueryHistorySearch() {
        Searches.setSearch("queryHistorySearch", {
            searchString: compact(_templateObject5, pageName)
        });
    })();

    var tabsControl = function () {
        return new Tabs($('#dashboard-form-tabs'), $('#dashboard-form-controls'));
    }();

    var queryHistoryPanel = new QueryHistoryTable($('#queryHistoryPanel'), 'queryHistorySearch', historyCollectionId, ['Actions', '_time', 'Search query', 'Field to predict', 'Method', 'Withhold', 'Forecast next k values', 'Confidence Interval', 'Period', 'R² Statistic', 'RMSE', '# of outliers'], submitButtonText, function (params, autostart) {
        var savedSearch = {};
        savedSearch.value = params.data["row.search_query"];
        savedSearch.earliestTime = params.data["row.earliest_time"];
        savedSearch.latestTime = params.data["row.latest_time"];
        savedSearch.algorithm = params.data["row.method"];
        savedSearch.fieldToPredict = params.data["row.field_to_predict"];
        savedSearch.holdback = params.data["row.withhold"];
        savedSearch.by_clause = params.data["row.by_clause"];
        savedSearch.exclude = params.data["row.exclude"];
        savedSearch.confidenceInterval = params.data["row.confidence_interval"];
        savedSearch.period = params.data["row.period"];
        savedSearch.span = params.data["row.span"];
        savedSearch.nonnegative = params.data["row.nonnegative"];
        savedSearch.metric = params.data["row.metric"];
        savedSearch.learning = params.data["row.learning"];
        savedSearch.autostart = autostart;

        loadSavedSearch(savedSearch);
    });

    var searchBarControl = function () {
        var searchBarControl = $('#searchBarControl');

        searchBarControl.parent().prev('label').tooltip({
            //TODO
            title: 'The events returned should contain a "_time" field with UNIX timestamp values and at least one other numeric field.'
        });

        return new SearchBarWrapper({
            "id": "searchBarControl",
            "managerid": "searchBarSearch",
            "el": searchBarControl,
            "autoOpenAssistant": false
        }, {
            "id": "searchControlsControl",
            "managerid": "searchBarSearch",
            "el": $("#searchControlsControl")
        }, function () {
            Forms.clearChoiceView(predictFieldsControl, true);
            Forms.unsetToken("predictFieldsToken");

            var searchBarSearch = Searches.getSearchManager("searchBarSearch");

            baseSearchString = this.searchBarView.val();
            baseTimerange = this.searchBarView.timerange.val();

            searchBarSearch.settings.unset("search");
            searchBarSearch.settings.set("search", baseSearchString);
            searchBarSearch.search.set(baseTimerange);

            updateForm();
        });
    }();

    var predictFieldsControl = function () {
        var predictFieldsControl = new DropdownView({
            "id": "predictFieldsControl",
            "el": $("#predictFieldsControl"),
            "managerid": "predictionFieldsSearch",
            "labelField": "column",
            "valueField": "column",
            showClearButton: false
        });

        predictFieldsControl.$el.prev('label').tooltip({
            title: 'Select the numeric field to forecast.'
        });

        predictFieldsControl.on("datachange", function () {
            if (currentSampleSearch != null) {
                var choices = Forms.getChoiceViewChoices(predictFieldsControl);

                if (choices.indexOf(currentSampleSearch.fieldToPredict) >= 0) {
                    predictFieldsControl.val(currentSampleSearch.fieldToPredict);
                } else {
                    // if the outlier variable can't be selected, we can remove the sample search since it's no longer relevant
                    currentSampleSearch = null;
                }
            }
        });

        predictFieldsControl.on("change", function (value) {
            Forms.setToken("predictFieldsToken", value);
            Forms.setToken('metricToken', value);

            updateForm();

            if (value != null) {
                if (currentSampleSearch != null) {
                    if (currentSampleSearch.autostart !== false) {
                        assistantControlsFooter.controls.submitButton.trigger('submit');
                    } else {
                        // if the sample search isn't auto-running, we want to remove it since it's no longer relevant
                        currentSampleSearch = null;
                    }
                }
            }
        });
        predictFieldsControl.render();

        return predictFieldsControl;
    }();

    var predictAlgorithmControl = function () {
        var predictAlgorithmControl = new DropdownView({
            "id": "predictAlgorithmControl",
            "el": $("#predictAlgorithmControl"),
            "labelField": "label",
            "valueField": "value",
            "selectFirstChoice": true,
            "showClearButton": false,
            "choices": [
                { "label": "maxLLP1 (seasonal local level with max confidence interval extended by max error)",
                    "value": "maxLLP1" },
                { "label": "rmseLLP1 (seasonal local level with max confidence interval extended by RMSE)",
                    "value": "rmseLLP1" },
                { "label": "LLP1 (seasonal local level with max confidence interval)", "value": "LLP1" },

                { "label": "maxLLP5 (seasonal local level with trend with confidence interval extended by max error)",
                    "value": "maxLLP5" },
                { "label": "rmseLLP5 (seasonal local level with trend with confidence interval extended by RMSE)",
                    "value": "rmseLLP5" },
                { "label": "LLP5 (seasonal local level with trend)", "value": "LLP5" },

                { "label": "maxLLP2 (seasonal local level with LL with confidence interval extended by max error)",
                    "value": "maxLLP2" },
                { "label": "rmseLLP2 (seasonal local level with LL with confidence interval extended by RMSE)",
                    "value": "rmseLLP2" },
                { "label": "LLP2 (seasonal local level with LL)", "value": "LLP2" },

                { "label": "maxLLP (seasonal local level with confidence interval extended by max error)",
                    "value": "maxLLP" },
                { "label": "rmseLLP (seasonal local level with confidence interval extended by RMSE)",
                    "value": "rmseLLP" },
                { "label": "LLP (seasonal local level)", "value": "LLP" }
            ]
        });

        predictAlgorithmControl.$el.prev('label').tooltip({
            title: 'Select the appropriate method, which will depend on whether your data has long-term trends, periodicity, or both.'
        });

        predictAlgorithmControl.on("change", function (value) {
            Forms.setToken("predictAlgorithmToken", value);
        });
        predictAlgorithmControl.render();

        return predictAlgorithmControl;
    }();

    var holdbackControl = function () {
        var holdbackControl = new TextInputView({
            id: 'holdbackControl',
            el: $('#holdbackControl')
        });

        holdbackControl.$el.prev('label').tooltip({
            html: true,
            title: 'Withholding allows us to compare the forecast against known values (those that were withheld). ' +
            'This comparison is done using the R<sup>2</sup> and RMSE statistics and the Forecast Outliers, below.'
        });

        holdbackControl.on("change", function (value) {
            var holdback = parseInt(value, 10);

            if (isNaN(holdback) || holdback < 0) {
                controlValidity.set(holdbackControl.id, false);
                Messages.setTextInputMessage(this, 'Withhold value must be a positive integer.');
            } else {
                controlValidity.set(holdbackControl.id, true);
                Messages.removeTextInputMessage(this);

                //convert to integer if the value is float
                holdbackControl.val(holdback);
                Forms.setToken('holdbackToken', holdback);

            }

            updateForm();
        });

        holdbackControl.render();

        return holdbackControl;
    }();

    var timespanControl = function () {
        var timespanControl = new TextInputView({
            id: 'timespanControl',
            el: $('#timespanControl')
        });

        timespanControl.$el.prev('label').tooltip({
            html: true,
            title: 'time window size of buckets passed to predict function'
        });

        var ts_update = function () {
            var value = timespanControl.val();
            var parsed = /^(\d+)(.)$/g.exec(value);

            if (!value) {
                // Forms.setToken('trainingSplitToken', '-Infinity');
                controlValidity.set(timespanControl.id, false);
                Messages.setTextInputMessage(timespanControl, 'Field cannot be empty');
            }
            else if (Array.isArray(parsed) && parsed.length == 3) {
                var time_span = parseInt(parsed[1], 10);
                var span_unit = parsed[2];
                controlValidity.set(timespanControl.id, true);
                Messages.removeTextInputMessage(timespanControl);

                if (holdbackControl.val()) {
                    Forms.setToken('trainingSplitToken', time_span * holdbackControl.val() + span_unit);
                }
                else {
                    Forms.setToken('trainingSplitToken', '-Infinity');
                }

            } else {
                controlValidity.set(timespanControl.id, false);
                Messages.setTextInputMessage(timespanControl, 'Check timespan. Should be like 3h, 1m, etc');
            }

            updateForm();

        };

        timespanControl.on("change", ts_update);

        holdbackControl.on("change", ts_update);

        timespanControl.render();

        return timespanControl;
    }();


    var excludeControl = function () {
        var excludeControl = new CheckboxView({
            id: 'excludeControl',
            el: $('#excludeControl'),
            default: true
        });

        excludeControl.$el.parent('label').tooltip({
            title: 'Enables or disabled filtering of results by provided exclusions'
        });

        excludeControl.on("change", function (exclude) {
            Forms.setToken('excludeToken', exclude ? 'gdexclude | ' : '');
        });

        Forms.setToken('excludeToken', 'gdexclude | ');
        excludeControl.render();

        return excludeControl;
    }();


    var byClauseControl = function () {
        var byClauseControl = new TextInputView({
            id: 'byClauseControl',
            el: $('#byClauseControl'),
            placeholder: 'comma separated'
        });

        byClauseControl.$el.prev('label').tooltip({
            title: 'Comma separated field names to group prediction by'
        });

        byClauseControl.on("change", function (value) {

            if(value) {
                Forms.setToken('byClause', 'by ' + value);
            }
            else {
                Forms.setToken('byClause', '');
            }


            updateForm();
        });
        Forms.setToken('byClause', '');

        byClauseControl.render();

        return byClauseControl;
    }();

    var confidenceIntervalControl = function () {
        var confidenceIntervalControl = new TextInputView({
            id: 'confidenceIntervalControl',
            el: $('#confidenceIntervalControl')
        });

        confidenceIntervalControl.$el.prev('label').tooltip({
            title: 'Specify a value between 0 and 100, where a larger value means a greater tolerance for forecast uncertainty.'
        });

        confidenceIntervalControl.on("change", function (value) {
            var confidenceInterval = parseInt(value, 10);

            if (isNaN(confidenceInterval) || confidenceInterval < 0 || confidenceInterval >= 100) {
                controlValidity.set(confidenceIntervalControl.id, false);
                Messages.setTextInputMessage(this, 'Confidence interval percentile value must be a integer between 0 and 99.');
            } else {
                controlValidity.set(confidenceIntervalControl.id, true);
                Messages.removeTextInputMessage(this);

                //convert to integer if the value is float
                confidenceIntervalControl.val(confidenceInterval);
                Forms.setToken('confidenceIntervalToken', confidenceInterval);
            }

            updateForm();
        });

        confidenceIntervalControl.render();

        return confidenceIntervalControl;
    }();

    var periodValueControl = function () {
        var periodValueControl = new TextInputView({
            id: 'periodValueControl',
            disabled: false,
            el: $('#periodValueControl'),
            initialValue: 168
        });

        periodValueControl.on("change", function (value) {
            if (value == null) {
                Forms.setToken('periodValueToken', '');
            } else {
                var periodValue = parseInt(value, 10);
                Forms.setToken('periodValueToken', "period=" + periodValue);
            }
            updatePeriodValueControlValidity();
        });

        periodValueControl.render();

        return periodValueControl;
    }();

    function updatePeriodValueControlValidity() {
        if (periodValueControl != null) {
            var periodValue = periodValueControl.val();

            if (isNaN(periodValue) || periodValue <= 0) {
                controlValidity.set(periodValueControl.id, false);
                Messages.setTextInputMessage(periodValueControl, 'Period value must be a positive integer.');
            } else {
                controlValidity.set(periodValueControl.id, true);
                Messages.removeTextInputMessage(periodValueControl);
            }

            updateForm();
        }
    }

    var nonnegativeCheckboxControl = function () {
        var nonnegativeCheckboxControl = new CheckboxView({
            id: 'nonnegativeCheckboxControl',
            el: $('#nonnegativeCheckboxControl')
        });

        nonnegativeCheckboxControl.$el.parent('label').tooltip({
            title: 'True if metric cannot be negative'
        });

        nonnegativeCheckboxControl.on("change", function (value) {
            if (!value) {
                Forms.setToken('nonnegativeValueToken', 'f');
            } else {
                Forms.setToken('nonnegativeValueToken', 't');
            }
        });
        Forms.setToken('nonnegativeValueToken', 'f');
        nonnegativeCheckboxControl.render();
        return nonnegativeCheckboxControl;
    }();

    var metricControl = function () {
        var metricControl = new TextInputView({
            id: 'metricControl',
            el: $('#metricControl')
        });

        metricControl.$el.prev('label').tooltip({
            title: 'Aggratation specification supported by timechart command'
        });

        metricControl.on("change", function (value) {

            Forms.setToken('metricToken', value);
            if (metricControl.val()) {
                Messages.removeTextInputMessage(this);
            }

            // updateForm();
        });
        Forms.setToken('metric', '');

        metricControl.render();

        return metricControl;
    }();


    var windowControl = function () {
        var windowControl = new TextInputView({
            id: 'windowControl',
            el: $('#windowControl')
        });

        windowControl.$el.prev('label').tooltip({
            title: 'Length of learning cycle. Value like 1h/3w/6m/etc'
        });

        windowControl.on("change", function (value) {

            Forms.setToken('windowToken', value);
            if (windowControl.val()) {
                Messages.removeTextInputMessage(this);
            }

            // updateForm();
        });
        Forms.setToken('windowToken', '');

        windowControl.render();

        return windowControl;
    }();

    function getAce() {
        return ace.edit($('#searchBarControl .ace_editor')[0]);
    }

    var $makeTimechartControl = function() {

        var $timechartControl = $('#makeTimeChart');

        function disabled() {
            if(controlValidity.getAll() && metricControl.val() ) {
                $timechartControl.prop("disabled", false);
            }
            else {
                $timechartControl.prop("disabled", true);
            }
        }

        $(document).on('generalValidity', disabled);
        metricControl.on('change', disabled);
        predictFieldsControl.on('change', disabled);

        function makeTimechart() {
            var timechart;
            if (byClauseControl.val()) {
                timechart = "" +
                    'eval by_clause= ' + byClauseControl.val().split(',').join(' + "#" + ') + '\n' +
                    '| timechart span=' + timespanControl.val() +
                    '  limit=0 useother=False partial=False ' + metricControl.val() + ' as metric by by_clause' + '\n' +
                    '| untable _time, by_clause, metric ' +'\n' +
                    '| eval splet=split(by_clause,"#") ' +'\n' +
                    '| eval task_type=mvindex(splet,0)' +'\n' +
                    '| eval mode=mvindex(splet,1) ' +'\n' +
                    '| fields - splet\n' ;
            }
            else {
                timechart = 'timechart ' + metricControl.val() + ' as metric span=' + timespanControl.val() +
                    ' limit=0 useother=False partial=False ';
            }

            var aceSearch = getAce();
            var current = aceSearch.getValue();

            if (current.indexOf('timechart') != -1) {
                aceSearch.setValue(current.replace(/timechart[^\|]+/m, timechart));
            }
            else {
                aceSearch.setValue(current + (current.endsWith('\n') ? '': '\n') + '| ' + timechart);
            }
        }
        $timechartControl.on("click", makeTimechart);
        return $timechartControl;

    }();


    var $scheduleAlertControl = function() {

        var $alertButton = $('#scheduleAlert');

        function disabled() {
            // has timechart present in search or prediction field selected
            if(controlValidity.getAll() && windowControl.val() && (
                metricControl.val() || predictFieldsControl.val())) {
                $alertButton.prop('disabled', false);
            }
            else {
                $alertButton.prop('disabled', true);
            }
        }

        $(document).on('generalValidity', disabled);
        searchBarControl.searchBarView.on('change', disabled);
        windowControl.on('change', disabled);
        predictFieldsControl.on('change', disabled);

        $alertButton.on('click', function () {

            var forecastVizAlertModal = new Modal('forecastVizAlertModal', {
                title: 'Schedule an alert',
                destroyOnHide: false,
                type: 'wide'
            });
            var aceSearch = getAce();
            var searchString = Forms.parseTemplate([aceSearch.getValue()].concat([
                compact(_templateObjectAlert),
                "| where outliersNum>0",
                "| table _time, outliers, by_clause",
                "| dedup consecutive=t outliers"
            ]).join('\n'));

            var $queryArea = $('<textarea>').width('100%').height('200px');
            $queryArea.text(searchString);

            forecastVizAlertModal.body.addClass('mlts-modal-form-inline').append(
                // specifically not using .html() because we can't trust the value of the token (since it's user-submitted data)
                $('<p>').append('Alert me when outliers found'),
                $('<br>'),
                $queryArea
            );

            var cancelButton = $('<button>').addClass('mlts-modal-cancel').attr({
                type: 'button',
                'data-dismiss': 'modal'
            }).addClass('btn btn-default mlts-modal-cancel').text('Cancel');

            var submitButtonText = 'Next';
            var submitButton = $('<button>').attr({
                type: 'button'
            }).addClass('btn btn-primary mlts-modal-submit').text(submitButtonText);

            forecastVizAlertModal.footer.append(cancelButton, submitButton.on('click', function () {
                forecastVizAlertModal.removeAlert();

                submitButton.attr('disabled', true).text('Loading...');

                new AlertModal({
                    searchString: $queryArea.val()
                }).done(function (alertModal) {
                    // check if the user has quit the preceding modal before the AlertModal is done loading
                    if (forecastVizAlertModal.isShown()) {
                        forecastVizAlertModal.hide();
                        alertModal.render().appendTo($('body')).show();
                    } else {
                        alertModal.remove();
                    }
                }).fail(function (message) {
                    forecastVizAlertModal.setAlert(message);
                    submitButton.attr('disabled', false).text(submitButtonText);
                });
            }));

            forecastVizAlertModal.$el.on('show.bs.modal', function () {
                submitButton.attr('disabled', false).text(submitButtonText);

                forecastVizAlertModal.removeAlert();

            });

            forecastVizAlertModal.show();
        });

        return $alertButton;

    }();

    var assistantControlsFooter = function () {
        var assistantControlsFooter = new AssistantControlsFooter($('#assistantControlsFooter'), submitButtonText);

        assistantControlsFooter.controls.submitButton.on('submit', function () {
            activatePredictionSearch();
        });

        return assistantControlsFooter;
    }();

    var regressionStatisticsPanel = function () {
        var footer = new AssistantPanelFooter($('#regressionStatisticsPanel'));

        // gives us somewhere to attach the spinner
        footer.spinnerAnchor = footer.footer.parent('.panel-body').parent('.dashboard-element');

        return footer;
    }();

    var r2StatisticPanel = function () {
        return new AssistantPanel({
            el: $('#r2StatisticPanel'),
            title: 'R<sup>2</sup> Statistic',
            tooltip: {
                html: true,
                title: 'The square of the correlation coefficient between the forecasted and actual values. Better forecasts have an R<sup>2</sup> closer to 1.'
            },
            viz: SingleView,
            vizOptions: {
                id: 'r2StatisticViz',
                managerid: 'regressionstatisticsSearch',
                field: 'rSquared',
                numberPrecision: '0.0000',
                height: 30
            },
            panelWrapper: false,
            footerButtons: false
        });
    }();

    var rootMeanSquaredErrorStatisticPanel = function () {
        return new AssistantPanel({
            el: $('#rootMeanSquaredErrorStatisticPanel'),
            title: 'Root Mean Squared Error (RMSE)',
            tooltip: 'The quadratic mean of the prediction errors (residuals). Better models have a smaller RMSE.',
            viz: SingleView,
            vizOptions: {
                id: 'rootMeanSquaredErrorStatisticViz',
                managerid: 'regressionstatisticsSearch',
                field: 'RMSE',
                numberPrecision: '0.00',
                height: 30
            },
            panelWrapper: false,
            footerButtons: false
        });
    }();

    var predictionOutliersStatisticPanel = function () {
        var assistantPanel = new AssistantPanel({
            el: $('#predictionOutliersStatisticPanel'),
            title: 'Forecast Outliers',
            tooltip: 'Number of values in the test period that fall outside the confidence interval.',
            viz: SingleView,
            vizOptions: {
                id: 'predictionOutliersStatisticViz',
                managerid: 'predictionOutliersSearch',
                field: 'count',
                numberPrecision: '0',
                height: 30
            }
        });

        assistantPanel.plotOutliersOverTimeButton = $('<button>').addClass('btn btn-default mlts-plot-outliers-over-time').text('Plot Outliers Over Time');
        assistantPanel.footer.append(assistantPanel.plotOutliersOverTimeButton);

        return assistantPanel;
    }();

    var forecastPanel = function () {
        var ForecastViz = VisualizationRegistry.getVisualizer(appName, 'ForecastViz');

        return new AssistantPanel({
            el: $('#forecastPanel'),
            title: 'Forecast',
            tooltip: 'The input data, forecast, and confidence intervals. Values to the right of the "training-test split" line were not shown to the algorithm. Narrower confidence intervals mean more confident predictions.',
            viz: ForecastViz,
            vizOptions: {
                id: 'forecastViz',
                managerid: searchManagerId
            }
        });
    }();

    var datasetPreviewTable = function () {
        return new TableView({
            id: 'datasetPreviewTable',
            el: $('#datasetPreviewPanel'),
            managerid: 'searchBarSearch',
            count: 30
        });
    }();

    function getVizQuery() {
        var sharedSearchArray = arguments.length <= 0 || arguments[0] === undefined ? [predictSearchString] : arguments[0];

        var vizQueryArray = [baseSearchString].concat(sharedSearchArray);
        var vizQuerySearch = DrilldownLinker.createSearch(vizQueryArray, baseTimerange);

        return [vizQueryArray, vizQuerySearch];
    }

    function loadSavedSearch(sampleSearch) {
        tabsControl.activate('newForecast');

        currentSampleSearch = _.extend({}, {
            algorithm: 'maxLLP1',
            holdback: 24,
            confidenceInterval: 95
        }, sampleSearch);

        searchBarControl.setProperties(sampleSearch.value, sampleSearch.earliestTime, sampleSearch.latestTime);

        var holdback = currentSampleSearch.holdback;
        var algorithm = currentSampleSearch.algorithm;
        var confidenceInterval = currentSampleSearch.confidenceInterval;

        //Assign pre-canned parameters to each input
        if (currentSampleSearch.period != null && currentSampleSearch.period != 0) {
            var period = currentSampleSearch.period;
            periodValueControl.val(period);
        } else {
            periodValueControl.settings.unset("value");
            periodValueControl.settings.set("disabled", true);
        }
        holdbackControl.val(holdback);
        timespanControl.val(currentSampleSearch.span);
        nonnegativeCheckboxControl.val(currentSampleSearch.nonnegative);
        metricControl.val(currentSampleSearch.metric);
        windowControl.val(currentSampleSearch.learning);
        predictAlgorithmControl.val(algorithm);
        confidenceIntervalControl.val(confidenceInterval);
    }

    function activatePredictionSearch() {
        // the controlValidity.getAll() check is intentionally made here so that the user can try to submit the form even with empty fields
        // the submission will fail and they'll see the appropriate errors
        if (!assistantControlsFooter.getDisabled() && controlValidity.getAll()) {
            currentSampleSearch = null;

            Searches.startSearch(searchManagerId);
        }
    }

    function updateForm(newIsRunningValue) {
        // optionally set a new value for isRunning
        if (newIsRunningValue != null) isRunning = newIsRunningValue;

        predictFieldsControl.settings.set('disabled', isRunning);
        predictAlgorithmControl.settings.set('disabled', isRunning);
        // don't re-enable windowSizeControl if it's disabled by windowedAnalysisCheckboxControl
        periodValueControl.settings.set('disabled', isRunning);
        nonnegativeCheckboxControl.settings.set('disabled', isRunning);
        holdbackControl.settings.set('disabled', isRunning);
        timespanControl.settings.set('disabled', isRunning);
        excludeControl.settings.set('disabled', isRunning);
        byClauseControl.settings.set('disabled', isRunning);
        metricControl.settings.set('disabled', isRunning);
        windowControl.settings.set('disabled', isRunning);
        confidenceIntervalControl.settings.set('disabled', isRunning);


        if (isRunning) {
            assistantControlsFooter.setDisabled(true);
            assistantControlsFooter.controls.submitButton.text('Forecasting...');
        } else {
            var predictFieldsToken = Forms.getToken('predictFieldsToken');
            var fieldsValid = predictFieldsToken != null && predictFieldsToken.length > 0 && controlValidity.getAll()
                && ! byClauseControl.val();

            assistantControlsFooter.setDisabled(!fieldsValid);
            assistantControlsFooter.controls.submitButton.text(submitButtonText);
        }
    }

    $(document).on('generalValidity', function () {updateForm(isRunning)});

    function showErrorMessage(errorMessage) {
        var errorDisplay$El = $("#errorDisplay");
        Messages.setAlert(errorDisplay$El, errorMessage, undefined, undefined, true);
    }

    function hideErrorMessage() {
        var errorDisplay$El = $("#errorDisplay");
        Messages.removeAlert(errorDisplay$El, true);
    }

    function hidePanels() {
        Forms.unsetToken('showResultPanelsToken');
    }

    function showPanels() {
        Forms.setToken('showResultPanelsToken', true);
    }

    // update validity for the initial state of the period controls
    updatePeriodValueControlValidity();

    setupSearches();

    // load canned searches from URL bar parameters
    (function setInputs() {
        var searchParams = ParseSearchParameters(pageName);

        loadSavedSearch(searchParams);
    })();

    // disable the form on initial load
    setTimeout(updateForm, 0);

    tabsControl.activate('manageExclusions');
});
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define(['util/time', "components/controls/HighchartsBase", 'highcharts-more', "components/data/parameters/ColorPalette"], function (time, Highcharts, HighchartsMore, ColorPalette) {
    var ForecastPlot = function () {
        function ForecastPlot(container$El) {
            _classCallCheck(this, ForecastPlot);

            // not using $(el).highcharts() to avoid pulling in any Highcharts except the one explicitly loaded with RequireJS
            this.chart = new Highcharts.Chart({
                chart: {
                    renderTo: container$El.get(0)
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime'
                },
                lang: {
                    noData: "You must use the `modvizgdpredict` macro to generate data for this visualization"
                },
                plotOptions: {
                    series: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                credits: false
            });
        }

        /**
         * Load data for charting. First argument fields are expected to be: _time, value, predictedValue, lower95, upper95
         * @param {Array}  data A 2d array of Splunk search results.
         * @param {string} variableToPredict
         * @param {number} holdback
         * @param {number} futurePoints
         */


        _createClass(ForecastPlot, [{
            key: 'setSeries',
            value: function setSeries(data, variableToPredict, holdback, futurePoints) {
                if (futurePoints == null) futurePoints = 0;
                if (holdback == null) holdback = 0;

                this.chart.xAxis[0].removePlotLine();

                if (futurePoints > 0 && data.length >= futurePoints + 1) {
                    var trainingPoints = data.length - futurePoints - 1;
                    var trainingEndTime = time.isoToDateObject(data[trainingPoints][0]).valueOf();

                    this.chart.xAxis[0].addPlotLine({
                        color: ColorPalette.getColorByIndex(4),
                        width: 2,
                        value: trainingEndTime,
                        dashStyle: 'DashDot',
                        label: {
                            text: 'training-test split',
                            textAlign: 'center',
                            verticalAlign: 'top',
                            rotation: 0,
                            x: 1
                        }
                    });
                }

                var realSeries = {
                    name: variableToPredict,
                    type: 'line',
                    data: [],
                    marker: { symbol: 'circle' },
                    zIndex: 1,
                    color: ColorPalette.getColorByIndex(26) //deep red
                };

                var predictionSeries = {
                    name: 'prediction',
                    dashStyle: 'shortdot',
                    data: [],
                    marker: { symbol: 'circle' },
                    zIndex: 1,
                    tooltip: { valueDecimals: 2 },
                    color: ColorPalette.getColorByIndex(6) //deep blue
                };

                var confidenceIntervalSeries = {
                    name: 'confidence interval',
                    data: [],
                    type: 'arearange',
                    lineWidth: 0,
                    tooltip: {
                        valueDecimals: 2,
                        pointFormat: '<span style="color:{series.color}">●</span> {series.name}: <b>{point.low}</b> to <b>{point.high}</b><br/>'
                    },
                    color: ColorPalette.getColorByIndex(25) //medium orange
                };

                var futureConfidenceIntervalSeries = {
                    name: 'future confidence interval',
                    data: [],
                    type: 'arearange',
                    lineWidth: 0,
                    tooltip: {
                        valueDecimals: 2,
                        pointFormat: '<span style="color:{series.color}">●</span> {series.name}: <b>{point.low}</b> to <b>{point.high}</b><br/>'
                    },
                    color: ColorPalette.getColorByIndex(7) //light green
                };

                data.forEach(function (datum, index) {
                    var _time = time.isoToDateObject(datum[0]).valueOf();
                    var realData = datum[1] == null ? null : parseFloat(datum[1]);
                    var pd = datum[2] == null ? null : parseFloat(datum[2]);
                    var lower95 = datum[3] == null ? null : parseFloat(datum[3]);
                    var upper95 = datum[4] == null ? null : parseFloat(datum[4]);

                    realSeries.data.push([_time, realData]);
                    predictionSeries.data.push([_time, pd]);

                    if (index < data.length - futurePoints + holdback) {
                        confidenceIntervalSeries.data.push([_time, lower95, upper95]);
                    }
                    if (index >= data.length - futurePoints + holdback - 1) {
                        // make up the gap between two intervals
                        futureConfidenceIntervalSeries.data.push([_time, lower95, upper95]);
                    }
                });

                while (this.chart.series.length > 0) {
                    this.chart.series[0].remove(false);
                }
                //add new series
                if (data != null && data.length > 0) {
                    this.chart.addSeries(realSeries, false);
                    this.chart.addSeries(predictionSeries, false);
                    this.chart.addSeries(confidenceIntervalSeries, false);
                    this.chart.addSeries(futureConfidenceIntervalSeries, false);
                }

                this.chart.redraw();
            }
        }, {
            key: 'reflow',
            value: function reflow() {
                if (this.chart != null) this.chart.reflow();
            }
        }]);

        return ForecastPlot;
    }();

    return ForecastPlot;
});
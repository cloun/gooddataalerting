"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// LinesPlot
define(["components/controls/HighchartsBase", "highcharts-downsample", "components/data/parameters/ColorPalette", "components/data/parameters/SimpleLegendAlign", "Options"], function (Highcharts, HighchartsDownsample, ColorPalette, SimpleLegendAlign, Options) {
    return (
        /**
         *
         * @param {element} container$El
         * @param {object}  options
         * @param {string}  [options.xAxisLabel]
         * @param {string}  [options.yAxisLabel]
         * @param {string}  [options.legendAlign] Can be "top", "bottom", "left", or "right"
         */
        function LinesPlot(container$El) {
            var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            _classCallCheck(this, LinesPlot);

            var valueDecimals = Options.getOptionByName("highchartsValueDecimals");

            // create chart - not using $(el).highcharts() to avoid pulling in any Highcharts except the one explicitly loaded with RequireJS
            this.chart = new Highcharts.Chart({
                chart: {
                    renderTo: container$El.get(0)
                },
                credits: false,
                title: {
                    text: ""
                },
                xAxis: {
                    title: {
                        text: options.xAxisLabel
                    }
                },
                yAxis: {
                    title: {
                        text: options.yAxisLabel
                    }
                },
                tooltip: {
                    formatter: function formatter() {
                        return this.series.name + ": " + parseFloat(this.y.toFixed(valueDecimals));
                    }
                },
                legend: SimpleLegendAlign(options.legendAlign)
            });

            /**
             *
             * @param {Points[]} seriesData
             * @param {object}   [options]
             * @param {string}   [options.xAxisLabel]
             * @param {string}   [options.yAxisLabel]
             * @param {string}   [options.xAxisType]
             */
            this.setSeries = function (seriesData) {
                var _this = this;

                var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

                var plotPointThreshold = Options.getOptionByName("plotPointThreshold");

                // remove old series from chart
                while (this.chart.series.length > 0) {
                    this.chart.series[0].remove(false);
                }

                if (options.xAxisType != null) this.chart.xAxis[0].update({ type: options.xAxisType });

                // add new series to chart
                seriesData.forEach(function (series, index) {
                    _this.chart.addSeries({
                        type: "line",
                        data: series.toDataArray(),
                        marker: {
                            enabled: false
                        },
                        name: series.getLabel(),
                        color: ColorPalette.getColorByIndex(index),
                        downsample: {
                            threshold: plotPointThreshold
                        },
                        zIndex: series.priority
                    }, false);
                });

                if (options.xAxisLabel != null) {
                    this.chart.xAxis[0].setTitle({
                        text: options.xAxisLabel
                    }, false);
                }

                if (options.yAxisLabel != null) {
                    this.chart.yAxis[0].setTitle({
                        text: options.yAxisLabel
                    }, false);
                }

                this.chart.redraw();
            };

            this.reflow = function () {
                if (this.chart != null) this.chart.reflow();
            };
        }
    );
});
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define(['util/time', "components/controls/HighchartsBase", 'highcharts-more', "highcharts-downsample", "components/data/parameters/ColorPalette", "components/data/parameters/SimpleLegendAlign", "Options", "components/controls/Messages"], function (time, Highcharts, HighchartsMore, HighchartsDownsample, ColorPalette, SimpleLegendAlign, Options, Messages) {
    return function OutliersPlot(container$El) {
        var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

        _classCallCheck(this, OutliersPlot);

        var chartOptions = {
            chart: {
                renderTo: container$El.get(0)
            },
            title: {
                text: ''
            },
            yAxis: {
                title: ''
            },
            credits: false,
            legend: SimpleLegendAlign(options.legendAlign)
        };

        // create chart - not using $(el).highcharts() to avoid pulling in any Highcharts except the one explicitly loaded with RequireJS
        this.chart = new Highcharts.Chart(chartOptions);

        this.setSeries = function (data, seriesName) {
            var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

            var plotPointThreshold = Options.getOptionByName("plotPointThreshold");
            var valueDecimals = Options.getOptionByName("highchartsValueDecimals");

            var firstPoint = data[0][0];
            var lastPoint = data.length > 1 ? data[data.length - 1][0] : null;

            // determine if the first element of the first and last data points is a timestamp
            var firstPointHasTimestamp = firstPoint != null && !isNaN(time.isoToDateObject(firstPoint).valueOf());
            var lastPointHasTimestamp = lastPoint != null && !isNaN(time.isoToDateObject(lastPoint).valueOf());

            // check if the data is time series data (real time series, not "Splunk imported a CSV and couldn't parse the timestamps so it set them all to the same one")
            var hasTimestamps = firstPointHasTimestamp && lastPointHasTimestamp && firstPoint !== lastPoint;

            this.chart.xAxis[0].update({ type: hasTimestamps ? 'datetime' : 'linear' });

            var mainSeries = {
                name: seriesName,
                color: ColorPalette.getColorByIndex(0),
                tooltip: {
                    valueDecimals: valueDecimals
                },
                enableMouseTracking: false,
                data: [],
                downsample: {
                    threshold: plotPointThreshold
                }
            };

            var outlierSeries = {
                name: seriesName,
                type: 'line',
                color: ColorPalette.getColorByIndex(1),
                turboThreshold: 0,
                tooltip: {
                    valueDecimals: valueDecimals
                },
                point: {
                    events: {
                        click: options.onClick || function () {}
                    }
                },
                marker: {
                    enabled: true,
                    symbol: 'circle'
                },
                lineWidth: 0,
                showInLegend: false,
                data: []
            };

            var confidenceSeries = {
                name: 'Confidence Interval',
                type: 'arearange',
                color: ColorPalette.getColorByIndex(0),
                lineWidth: 0,
                linkedTo: ':previous',
                fillOpacity: 0.3,
                zIndex: 0,
                tooltip: {
                    valueDecimals: valueDecimals,
                    pointFormat: '<span style="color:{series.color}">●</span> {series.name}: <b>{point.low}</b> to <b>{point.high}</b><br/>'
                },
                data: []
            };

            // build data for Highcharts from search results
            data.forEach(function (datum, index) {
                var xVal = hasTimestamps ? time.isoToDateObject(datum[0]).valueOf() : index;
                var yVal = parseFloat(datum[1]);
                if (isNaN(yVal)) yVal = null;

                var lower = parseFloat(datum[2]);
                if (isNaN(lower)) lower = null;

                var upper = parseFloat(datum[3]);
                if (isNaN(upper)) upper = null;

                var isOutlier = yVal != null && (lower != null && yVal < lower || upper != null && yVal > upper);

                mainSeries.data.push([xVal, yVal]);

                if (isOutlier) {
                    outlierSeries.data.push({
                        x: xVal,
                        y: yVal,
                        originalY: datum[1]
                    });
                }

                confidenceSeries.data.push([xVal, lower, upper]);
            });

            // remove old series
            while (this.chart.series.length > 0) {
                this.chart.series[0].remove(false);
            }

            // add new series
            this.chart.addSeries(mainSeries, false);
            this.chart.addSeries(outlierSeries, false);
            this.chart.addSeries(confidenceSeries, false);

            this.chart.redraw();
        };

        this.reflow = function () {
            if (this.chart != null) this.chart.reflow();
        };
    };
});
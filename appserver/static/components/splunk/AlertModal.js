'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define(['models/shared/Application', 'models/search/Report', 'models/search/Alert', 'splunkjs/mvc/sharedmodels', 'views/shared/alertcontrols/dialogs/saveas/Master'], function (AppModel, ReportModel, AlertModel, SharedModels, AlertDialog) {
    return function AlertModal() {
        var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        _classCallCheck(this, AlertModal);

        if (options.showSearch == null) options.showSearch = true;
        if (options.onHiddenRemove == null) options.onHiddenRemove = true;

        if (options.model == null) {
            options.model = {
                user: SharedModels.get('user'),
                serverInfo: SharedModels.get('serverInfo')
            };

            // we can't use the AppModel from SharedModels because it sets the "page" parameter to the current page
            // this is problematic because AlertDialog sets "request.ui_dispatch_view" to the "page" parameter
            // this in turn causes sets the Triggered Alert url to point back at the assistant, which can't load saved alert results
            // to combat this, we create our own AppModel and override its "page" parameter to always point at the search page, which can handle saved alert results
            var sharedAppModel = SharedModels.get('app');

            options.model.application = new AppModel({
                owner: sharedAppModel.get('owner'),
                root: sharedAppModel.get('root'),
                locale: sharedAppModel.get('locale'),
                app: sharedAppModel.get('app'),
                page: 'search'
            });
        }

        var reportDeferred = $.Deferred();

        if (options.model.report == null) {
            options.model.report = new ReportModel();

            // need to initialize the report model from the server or it ends up missing a bunch of fields
            options.model.report.fetch({
                data: {
                    app: options.model.application.get('app'),
                    owner: options.model.application.get('owner')
                },
                success: function (model, response) {
                    options.model.report.entry.content.set('search', options.searchString);
                    reportDeferred.resolve(new AlertDialog(options));
                }.bind(this),
                error: function (model, response) {
                    var message = response.responseJSON.messages ? response.responseJSON.messages[0] : null;
                    var messageText = 'An error occurred initializing your alert.';

                    if (message.text != null && message.text != '') messageText = message.text;

                    reportDeferred.reject(messageText);
                }.bind(this)
            });
        } else {
            options.model.report.entry.content.set('search', options.searchString);
            reportDeferred.resolve(new AlertDialog(options));
        }

        // instead of returning the AlertDialog directly, return a promise that resolves to the AlertDialog
        return reportDeferred;
    };
});
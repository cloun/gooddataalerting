'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define(['helpers/Printer'], function (Printer) {
    return function PrintButton() {
        _classCallCheck(this, PrintButton);

        return $('<a>').addClass('btn').on('click', function (e) {
            e.preventDefault();
            Printer.printPage();
        }).append($('<i>').addClass('icon-print icon-large')).tooltip({
            title: 'Print',
            placement: 'bottom'
        });
    };
});
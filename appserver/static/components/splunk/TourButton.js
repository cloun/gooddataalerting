'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

define(['module', 'components/splunk/TourLoader'], function (module, TourLoader) {
    var config = module.config();

    return function TourButton() {
        _classCallCheck(this, TourButton);

        return $('<a>').addClass('btn').on('click', function (e) {
            e.preventDefault();
            TourLoader.loadTour(config.pageName);
        }).append($('<i>').addClass('icon-question icon-large')).tooltip({
            title: 'Show Tour',
            placement: 'bottom'
        });
    };
});
'use strict';

define(['models/services/data/ui/Tour', 'splunkjs/mvc/sharedmodels'], function (TourModel, SharedModels) {
    return {
        /**
         * Uses Splunk's tour loading mechanisms to load an ImageTour on a dashboard
         * @param {string}  tourName
         * @param {boolean} [onlyIfNotViewed=false] If true, only loads the tour if viewed is not set in ui-tour.conf
         */
        loadTour: function loadTour(tourName) {
            var onlyIfNotViewed = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

            // Splunk changes where the ImageTour is located between Galaxy and Ivory, so we need fallback code to load the correct version
            require(['views/shared/tour/ImageTour'], function (ImageTourGalaxy) {
                loadTourInternal(ImageTourGalaxy);
            }, function (err) {
                require(['views/shared/tour/ImageTour/Master'], function (ImageTourIvory) {
                    loadTourInternal(ImageTourIvory);
                });
            });

            function loadTourInternal(ImageTour) {
                var appModel = SharedModels.get('app');
                var tourModel = new TourModel();
                var tourDeferred = $.Deferred();

                var app = appModel.get('app');
                var owner = appModel.get('owner');

                tourModel.bootstrap(tourDeferred, app, owner, tourName);

                tourModel.on('viewed', function () {
                    // from routers/Base.js updateTour()
                    // sets the tour's "viewed" state

                    var data = {};

                    if (tourModel.isNew()) {
                        data.app = tourModel.getTourApp();
                        data.owner = owner;
                    }

                    tourModel.save({}, {
                        data: data
                    });
                });

                tourDeferred.then(function () {
                    // if the tour doesn't exist, the tour name won't be set
                    var loadSuccess = tourModel.getName() === tourName;

                    // don't render the tour if onlyIfNotViewed is set
                    if (loadSuccess && (!onlyIfNotViewed || !tourModel.viewed() && onlyIfNotViewed)) {
                        var imageTour = new ImageTour({
                            model: {
                                tour: tourModel,
                                application: appModel
                            },
                            onHiddenRemove: true,
                            backdrop: 'static'
                        }).render();

                        // from shared/Page.js renderTour()
                        var wrapper = $('<div>').addClass('splunk-components image-tour');
                        $('body').append(wrapper);
                        wrapper.append(imageTour.el);
                        imageTour.show();
                    }
                });
            }
        }
    };
});
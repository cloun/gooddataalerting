'use strict';

// set the runtime environment, which controls cache busting
var runtimeEnvironment = 'production';

// set the build number, which is the same one being set in app.conf
var build = '1474492663887';

// get app and page names
var pathComponents = location.pathname.split('?')[0].split('/');
var appName = 'GoodDataAlerting';
var pageIndex = pathComponents.indexOf(appName);
var pageName = pathComponents[pageIndex + 1];

// path to the root of the current app
var appPath = "../app/" + appName;

// This code is originally from setRequireConfig.es6 and is injected into runPageScript.es6 and every visualization.es6 file using @setRequireConfig.es6@

var requireConfigOptions = {
    paths: {
        // app-wide path shortcuts
        "components": appPath + "/components",
        "vendor": appPath + "/vendor",
        "Options": appPath + "/components/data/parameters/Options",

        // requirejs loader modules
        "text": appPath + "/vendor/text/text",
        "json": appPath + "/vendor/json/json",
        "css": appPath + "/vendor/require-css/css",

        // jquery shims
        "jquery-ui-slider": appPath + "/vendor/jquery-ui-slider/jquery-ui.min",

        // highcharts shims
        "highcharts-amd": appPath + "/vendor/highcharts/highcharts.amd",
        "highcharts-more": appPath + "/vendor/highcharts/highcharts-more.amd",
        "highcharts-downsample": appPath + "/vendor/highcharts/modules/highcharts-downsample.amd",
        "no-data-to-display": appPath + "/vendor/highcharts/modules/no-data-to-display.amd"
    },
    shim: {
        "jquery-ui-slider": {
            deps: ["css!" + appPath + "/vendor/jquery-ui-slider/jquery-ui.min.css"]
        }
    },
    config: {
        "Options": {
            // app-wide options
            "options": {
                "appName": 'GoodDataAlerting',
                // the number of points that's considered "large" - how each plot handles this is up to it
                "plotPointThreshold": 1000,
                "maxSeriesThreshold": 20,
                "smallLoaderScale": 0.4,
                "largeLoaderScale": 1,
                "highchartsValueDecimals": 2,
                "defaultModelName": "default_model_name",
                "defaultRoleName": "default",
                "dashboardHistoryTablePageSize": 5
            }
        }
    }
};

require.config(requireConfigOptions);

// End of setRequireConfig.es6

// path to the script for the current page
var scriptPath = "components/pages/" + pageName;

var requireModules = ["jquery", "splunkjs/ready!", "css!" + appPath + "/style/app", "css!" + appPath + "/style/" + pageName];

var additionalConfigOptions = {
    config: {
    }
};

// additional config options and require modules for showcases
// TODO
// if (pageName.indexOf("forecast") >= 0) {
//     requireModules.push('components/data/sampleSearches/SampleSearchLoader');
//
//     additionalConfigOptions.config['components/data/sampleSearches/SampleSearchLoader'] = {
//         pageName: pageName
//     };
// }

additionalConfigOptions.urlArgs = "bust=" + (runtimeEnvironment === 'develop' ? Date.now() : build);

require.config(additionalConfigOptions);

// run page script
require(requireModules, function () {
    require(['splunkjs/mvc/simplexml/controller', "components/controls/DependencyChecker", 'components/data/parameters/ParseSearchParameters', 'components/splunk/PrintButton'], function (DashboardController, DependencyChecker, ParseSearchParameters, PrintButton) {
        DashboardController.onReady(function () {
            DashboardController.onViewModelLoad(function () {
                DependencyChecker.check(function (success) {
                    if (success) {
                        require([appPath + "/" + scriptPath]);

                        var buttonWrapper = $('<div>').addClass('mlts-header-buttons pull-right');
                        buttonWrapper.append(new PrintButton());
                        $('.dashboard-header').prepend(buttonWrapper);
                    }
                });

                // get the nav bar
                var header = $('#header');
                var appNavGalaxy = header.find('.shared-appbar-appnav');
                var appNavIvory = header.find('div[data-role="app-nav-container"]');

                // Ivory+ and Galaxy have different nav container elements so we need to use the right one
                var appNav = appNavIvory.length > 0 ? appNavIvory : appNavGalaxy.length > 0 ? appNavGalaxy : null;

                if (MutationObserver && appNav != null && appNav[0] != null) {
                    (function () {
                        var observer = new MutationObserver(function () {
                            // stop observing after the first change
                            observer.disconnect();

                            updateNav();
                        });

                        // an observer set up this way will catch Splunk's re-render of the nav bar, but not the changes in updateNav()
                        observer.observe(appNav[0], { childList: true });

                        // after 10 seconds, disconnect the observer if it hasn't been disconnected
                        setTimeout(function () {
                            observer.disconnect();console.log('dc');
                        }, 10000);
                    })();
                }

                function updateNav() {
                    // make external links in the nav bar open in a new window/tab and have the appropriate class
                    if (appNav != null) {
                        appNav.find('a').each(function (i, navItem) {
                            var navItem$El = $(navItem);
                            if (navItem$El.attr('href').indexOf('http') === 0) navItem$El.addClass('external').attr('target', '_blank');
                        });
                    }

                }

                updateNav();
            });
        });
    });
});
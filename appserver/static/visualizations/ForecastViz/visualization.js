"use strict";

var appPath = '../../app/GoodDataAlerting';

// This code is originally from setRequireConfig.es6 and is injected into runPageScript.es6 and every visualization.es6 file using @setRequireConfig.es6@

var requireConfigOptions = {
    paths: {
        // app-wide path shortcuts
        "components": appPath + "/components",
        "vendor": appPath + "/vendor",
        "Options": appPath + "/components/data/parameters/Options",

        // requirejs loader modules
        "text": appPath + "/vendor/text/text",
        "json": appPath + "/vendor/json/json",
        "css": appPath + "/vendor/require-css/css",

        // jquery shims
        "jquery-ui-slider": appPath + "/vendor/jquery-ui-slider/jquery-ui.min",

        // highcharts shims
        "highcharts-amd": appPath + "/vendor/highcharts/highcharts.amd",
        "highcharts-more": appPath + "/vendor/highcharts/highcharts-more.amd",
        "highcharts-downsample": appPath + "/vendor/highcharts/modules/highcharts-downsample.amd",
        "no-data-to-display": appPath + "/vendor/highcharts/modules/no-data-to-display.amd"
    },
    shim: {
        "jquery-ui-slider": {
            deps: ["css!" + appPath + "/vendor/jquery-ui-slider/jquery-ui.min.css"]
        }
    },
    config: {
        "Options": {
            // app-wide options
            "options": {
                "appName": 'GoodDataAlerting',
                // the number of points that's considered "large" - how each plot handles this is up to it
                "plotPointThreshold": 1000,
                "maxSeriesThreshold": 20,
                "smallLoaderScale": 0.4,
                "largeLoaderScale": 1,
                "highchartsValueDecimals": 2,
                "defaultModelName": "default_model_name",
                "defaultRoleName": "default",
                "dashboardHistoryTablePageSize": 5
            }
        }
    }
};

require.config(requireConfigOptions);

// End of setRequireConfig.es6

define(['jquery', 'vizapi/SplunkVisualizationBase', 'components/plots/ForecastPlot'], function ($, SplunkVisualizationBase, ForecastPlot) {
    return SplunkVisualizationBase.extend({

        initialize: function initialize() {
            SplunkVisualizationBase.prototype.initialize.apply(this, arguments);
            this.$el = $(this.el);
        },

        // search data params
        getInitialDataParams: function getInitialDataParams() {
            return {
                count: 0,
                outputMode: SplunkVisualizationBase.ROW_MAJOR_OUTPUT_MODE
            };
        },

        // Optionally implement to format data returned from search.
        // The returned object will be passed to updateView as 'data'
        formatData: function formatData(data) {
            if (data != null && data.fields != null) {
                data.fields = data.fields.map(function (field) {
                    return field.name;
                });
            }

            return data;
        },

        setupView: function setupView() {
            this.plot = new ForecastPlot(this.$el);
        },

        // Implement updateView to render a visualization.
        // 'data' will be the data object returned from formatData or from the search
        // 'config' will be the configuration property object
        updateView: function updateView(data, config) {
            var _this = this;

            var dataRows = data.rows;
            if (dataRows == null || dataRows.length === 0 || dataRows[0].length === 0) {
                return this;
            }

            if (data != null) {
                (function () {
                    var firstRow = data.rows[0];
                    var predictionField = 'prediction';
                    var newRows = [];

                    var params = {
                        fieldToPredict: firstRow[data.fields.indexOf('_var')],
                        futureTimespan: parseInt(firstRow[data.fields.indexOf('_ft')], 10),
                        holdback: parseInt(firstRow[data.fields.indexOf('_hb')], 10),
                        confidenceInterval: parseInt(firstRow[data.fields.indexOf('_ci')], 10)
                    };

                    var chartFields = ['_time', params.fieldToPredict, predictionField, "lower" + params.confidenceInterval + "(" + predictionField + ")", "upper" + params.confidenceInterval + "(" + predictionField + ")"];

                    var chartFieldIndices = chartFields.map(function (chartField) {
                        return data.fields.indexOf(chartField);
                    });

                    if (chartFieldIndices.indexOf(-1) < 0) {
                        newRows = data.rows.map(function (row) {
                            return chartFieldIndices.map(function (fieldIndex) {
                                return row[fieldIndex];
                            });
                        });
                    }

                    _this.plot.setSeries(newRows, params.fieldToPredict, params.holdback, params.futureTimespan);
                })();
            }
            return this;
        },

        // override to respond to re-sizing events
        reflow: function reflow() {
            if (this.plot != null) this.plot.reflow();
        }

    });
});
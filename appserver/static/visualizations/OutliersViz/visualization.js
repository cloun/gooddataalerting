"use strict";

var appPath = '../../app/GoodDataAlerting';

// This code is originally from setRequireConfig.es6 and is injected into runPageScript.es6 and every visualization.es6 file using @setRequireConfig.es6@

var requireConfigOptions = {
    paths: {
        // app-wide path shortcuts
        "components": appPath + "/components",
        "vendor": appPath + "/vendor",
        "Options": appPath + "/components/data/parameters/Options",

        // requirejs loader modules
        "text": appPath + "/vendor/text/text",
        "json": appPath + "/vendor/json/json",
        "css": appPath + "/vendor/require-css/css",

        // jquery shims
        "jquery-ui-slider": appPath + "/vendor/jquery-ui-slider/jquery-ui.min",

        // highcharts shims
        "highcharts-amd": appPath + "/vendor/highcharts/highcharts.amd",
        "highcharts-more": appPath + "/vendor/highcharts/highcharts-more.amd",
        "highcharts-downsample": appPath + "/vendor/highcharts/modules/highcharts-downsample.amd",
        "no-data-to-display": appPath + "/vendor/highcharts/modules/no-data-to-display.amd"
    },
    shim: {
        "jquery-ui-slider": {
            deps: ["css!" + appPath + "/vendor/jquery-ui-slider/jquery-ui.min.css"]
        }
    },
    config: {
        "Options": {
            // app-wide options
            "options": {
                "appName": 'GoodDataAlerting',
                // the number of points that's considered "large" - how each plot handles this is up to it
                "plotPointThreshold": 1000,
                "maxSeriesThreshold": 20,
                "smallLoaderScale": 0.4,
                "largeLoaderScale": 1,
                "highchartsValueDecimals": 2,
                "defaultModelName": "default_model_name",
                "defaultRoleName": "default",
                "dashboardHistoryTablePageSize": 5
            }
        }
    }
};

require.config(requireConfigOptions);

// End of setRequireConfig.es6

define(['vizapi/SplunkVisualizationBase', 'components/plots/OutliersPlot'], function (SplunkVisualizationBase, OutliersPlot) {
    function getConfig(viz) {
        var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
        var propName = arguments[2];

        var namespace = viz.getPropertyNamespaceInfo().propertyNamespace;
        return config["" + namespace + propName];
    }

    return SplunkVisualizationBase.extend({
        initialize: function initialize() {
            SplunkVisualizationBase.prototype.initialize.apply(this, arguments);
        },
        getInitialDataParams: function getInitialDataParams() {
            return {
                outputMode: SplunkVisualizationBase.ROW_MAJOR_OUTPUT_MODE
            };
        },
        setupView: function setupView() {
            this.plot = new OutliersPlot($(this.el));
        },
        updateView: function updateView(data) {
            var _this = this;

            var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

            if (data != null && data.fields != null && data.fields.length > 0) {
                (function () {
                    var xAxisLabel = data.fields[1] != null ? data.fields[1].name : '';

                    var _onClick = getConfig(_this, config, 'onClick');

                    _this.plot.setSeries(data.rows, xAxisLabel, {
                        onClick: function onClick() {
                            if (typeof _onClick === 'function') {
                                _onClick(this);
                            }
                        }
                    });
                })();
            }
        },
        reflow: function reflow() {
            if (this.plot != null) this.plot.reflow();
        }
    });
});
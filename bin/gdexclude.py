import os
import re
import sys
from collections import defaultdict
import splunk.Intersplunk
import time

sys.path.append(os.getcwd() + '/splunk-sdk')
from splunklib import client


class Exclusion(object):
    def __init__(self, time_from, time_to, hosts=None):
        if not hosts:
            hosts = {}
        else:
            hosts = set(hosts)
        self.time_from = time_from
        self.time_to = time_to
        self.hosts = hosts
        self._hosts_wc = {h for h in hosts if '*' in h}
        self._hosts_wo = {h for h in hosts if '*' not in h}

    def __repr__(self):
        return unicode(self.__dict__)

    def matches(self, event):
        t_matches = True
        if self.time_from and float(event['_time']) < self.time_from:
            t_matches = False
        if self.time_to and float(event['_time']) > self.time_to:
            t_matches = False
        if self.hosts:
            if event['host'] in self._hosts_wo:
                return t_matches
            elif [re.match(re.compile(h), event['host']) for h in self._hosts_wc]:
                return t_matches
            else:
                return False
        return t_matches


class Excluder(object):
    def __init__(self, token, search_command):
        self.token = token
        self.search_command = search_command
        self._init_cache()
        self.service = client.connect(token=token, owner='nobody', app='GoodDataAlerting')
        for row in self.service.kvstore['forecast_exclusions'].data.query():
            self.exclusions_dict[row['_key']] = Exclusion(row['range_from'], row['range_to'], row['hosts'])
        self.last_checked = 0
        # for now dumb month check
        self.actual_from = time.time() - 30 * 24 * 3600
        self.define_range()
        self.update_exclusions()

    def _init_cache(self):
        self.exclusions_dict = {}
        self.exclusions_dict_w_host = defaultdict(list)
        self.exclusions_other = []

    def define_range(self):
        pass
        # todo parse search to find out how long predict takes

    def update_exclusions(self):
        # storage reset
        self._init_cache()
        for row in self.service.kvstore['forecast_exclusions'].data.query():
            self.exclusions_dict[row['_key']] = Exclusion(row['range_from'], row['range_to'], row['hosts'])
        # cleaning outdated
        # TODO
        # self.exclusions_dict = {k: v for k, v in self.exclusions_dict.items()
        #                         if v.time_from > self.actual_from or v.time_to > self.actual_from}
        for k, v in self.exclusions_dict.items():
            if v.hosts:
                for h in v.hosts:
                    if '*' in h:
                        self.exclusions_other.append(v)
                    else:
                        self.exclusions_dict_w_host[h].append(v)
        self.exclusions_other = [v for k, v in self.exclusions_dict.items() if not v.hosts]

    def should_be_excluded(self, result):
        if 'host' in result:
            if result['host'] in self.exclusions_dict_w_host:
                for excl in self.exclusions_dict_w_host[result['host']]:
                    if excl.matches(result):
                        return True
        for excl in self.exclusions_other:
            if excl.matches(result):
                return True
        return False

    def filter(self, results):
        filtered = []
        last_checked = time.time()
        for r in results:
            if time.time() < last_checked + 60:
                self.update_exclusions()
            if not self.should_be_excluded(r):
                filtered.append(r)
        return filtered

if __name__ == "__main__":
    try:
        headers = {}
        results = splunk.Intersplunk.readResults(sys.stdin, headers)
        excluder = Excluder(headers['sessionKey'], headers['search'])
        splunk.Intersplunk.outputResults(excluder.filter(results))
    except StandardError as e:
        from traceback import format_exc
        splunk.Intersplunk.parseError(str(format_exc()))

# todo consider using for rerunning search if range in the past was excluded
# /Users/sergii/Desktop/good data/GoodDataApp/bin/splunk-sdk/examples/search.py

import os
import sys
from collections import defaultdict
from traceback import format_exc

sys.path.append(os.getcwd() + '/splunk-sdk')
sys.path.append(os.getcwd() + '/../../search/bin')

import splunk.Intersplunk

from splunk.util import OrderedDict
from predict import parseOps, predictAll, readSearchResults
import splunk.stats_util.statespace as statespace


# if we want to make tabled output from untabled, we need different field names
def format_field_name(input, grp_tuple):
    grp_str = '(' + '-'.join(grp_tuple) + ')'
    out = []
    for i in input:
        new_ord_dict = []
        for item in i.items():
            if item[0].startswith('_predicted'):
                new_ord_dict.append((item[0]+grp_str, item[1]+grp_str))
            elif item[0].startswith('_upper'):
                f = item[0].replace('_upper', '')
                new_ord_dict.append((
                    item[0]+grp_str,
                    item[1].replace(i['_predicted' + f], i['_predicted' + f]+grp_str)))
            elif item[0].startswith('_lower'):
                f = item[0].replace('_lower', '')
                new_ord_dict.append((
                    item[0]+grp_str,
                    item[1].replace(i['_predicted' + f], i['_predicted' + f]+grp_str)
                ))
            else:
                new_ord_dict.append(item)
        out.append(OrderedDict(new_ord_dict))
        return out


def aggregated_predict(args, input, tmode):
    grouped_results = defaultdict(list)
    for r in input:
        grp = tuple([r.get(gf, '') for gf in group_fields])
        grouped_results[grp] += [r]
    merged = []
    for grp, filtered_res in grouped_results.items():
        forecaster = parseOps(args)
        readSearchResults(results, forecaster)
        for fc in forecaster:
            data_end = fc.numvals - fc.holdback
            if data_end < statespace.LL.least_num_data():
                fc.output(results)
                break
            else:
                fc.setModel()
                fc.predict()
                fc.output(results)
        if tmode == 'tabled':
            filtered_res = format_field_name(filtered_res, grp)
            merged = [OrderedDict(m.items() + r.items()) for m, r in zip(merged, filtered_res)]
        else:
            merged.extend(filtered_res)
    return merged


def patch_confidence_intervals(predicted_values, rmses, nonneg):
    up = {}
    lo = {}
    by_fields = []
    for k, v in predicted_values[0].items():
        if k.startswith('_predicted'):
            field = k.replace('_predicted', '')
            by_fields.append(field)
            up[predicted_values[0]['_upper' + field]] = field
            lo[predicted_values[0]['_lower' + field]] = field

    for p in predicted_values:
        for k in p.keys():
            if k in up:
                p[k] = float(p[k]) + rmses[up[k]]
            elif k in lo:
                low = float(p[k]) - rmses[lo[k]]
                p[k] = low if not nonneg else (low if low > 0 else 0)
    return predicted_values


def calculate_multiple_statistics(predicted_values, stat_function):
    values = defaultdict(list)
    predictions = defaultdict(list)
    prediction_fields = {}
    by_fields = []
    # getting names of predicted fields
    for k, v in predicted_values[0].items():
        if k.startswith('_predicted'):
            field = k.replace('_predicted', '')
            by_fields.append(field)
            prediction_fields[field] = v
    # building arrays of values and predictions and put them to corresponding field name
    for patched in predicted_values:
        for f in by_fields:
            # we are not interested in error on points with not data
            if patched[f] != '':
                values[f] += [patched[f]]
                predictions[f] += [patched[prediction_fields[f]]]
    rmse = {}
    for f in by_fields:
        rmse[f] = stat_function(values[f], predictions[f])
    return rmse


def rmse_stat(values, predictions):
    return sum([(float(p)-float(v))**2 for p, v in zip(predictions, values)])/len(values)


def max_stat(values, predictions):
    return max([float(p)-float(v) for p, v in zip(predictions, values)])


def patch_with_simple_predict(args, input):
    forecaster = parseOps(args)
    predictAll(forecaster, input)


def patch_is_outlier(input):
    fields = []
    predicted_fields = {}
    for k, v in input[0].items():
        if k.startswith('_predicted'):
            field = k.replace('_predicted', '')
            fields += [field]
            predicted_fields[field] = v
    for i in input:
        i['outliersNum'] = 0
        i['outliers'] = []
        for f in fields:
            if i[f] != '':
                if not float(i[i['_lower'+f]]) <= float(i[f]) <= float(i[i['_upper'+f]]):
                    i['outliersNum'] += 1
                    i['outliers'] += [f]


def parse_argv():
    grp_fields = []
    args = sys.argv[1:]
    try:
        by_from = args.index('by')
        by_to = 0
    except ValueError:
        pass
    else:
        for i, arg in enumerate(args[by_from+1:]):
            if '=' in arg:
                break
            else:
                grp_fields.append(arg)
            by_to = i
        args = [v for i, v in enumerate(args) if i not in range(by_from, by_from+by_to+2)]
    alg = ''
    tmode = 'untabled'
    nonnegative = False
    del_options = []
    for i, a in enumerate(args):
        if a.startswith('algorithm'):
            alg = a.split('=')[1].strip()
            del_options.append(i)
        elif a.startswith('nonnegative'):
            nonnegative = a.split('=')[1].strip() == 't'
        elif a.startswith('tmode'):
            tmode = a.split('=')[1].strip()
            del_options.append(i)

    if alg in ('LLP', 'rmseLLP', 'maxLLP'):
        args.append('algorithm=LLP')
    elif alg in ('LLP1', 'rmseLLP1', 'maxLLP1'):
        args.append('algorithm=LLP1')
    elif alg in ('LLP2', 'rmseLLP2', 'maxLLP2'):
        args.append('algorithm=LLP2')
    elif alg in ('LLP5', 'rmseLLP5', 'maxLLP5'):
        args.append('algorithm=LLP5')
    else:
        splunk.Intersplunk.parseError('provided algorithm option is not supported.')
    del_options.reverse()
    for d in del_options:
        del args[d]
    return args, grp_fields, alg, nonnegative, tmode


if __name__ == "__main__":
    try:
        argv, group_fields, algor, nonneg, tmode = parse_argv()
        results = splunk.Intersplunk.readResults(None, None, False)
        if group_fields:
            output = aggregated_predict(argv, results, tmode)
        else:
            patch_with_simple_predict(argv, results)
            output = results
        if output:
            if algor in ('rmseLLP', 'rmseLLP1', 'rmseLLP2', 'rmseLLP5'):
                patch_confidence_intervals(results, calculate_multiple_statistics(output, rmse_stat), nonneg)
            elif algor in ('maxLLP', 'maxLLP1', 'maxLLP2', 'maxLLP5'):
                patch_confidence_intervals(results, calculate_multiple_statistics(output, max_stat), nonneg)
            patch_is_outlier(output)
        splunk.Intersplunk.outputResults(output)
    except StandardError as e:
        splunk.Intersplunk.parseError(format_exc())

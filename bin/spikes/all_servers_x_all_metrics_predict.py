#!/usr/bin/env python2.7
import csv

lookup_location = '/Applications/Splunk/etc/apps/GoodDataAlerting/lookups/whole_deployment_2w.csv'

lookup_query = """
index=good  sourcetype="gooduploadcsv"
 | where _time>info_min_time and _time<info_max_time
| eval by_clause=task_type+ "#" + mode
| eval single_throughput=csv_fsize_kB/duration_sec
| timechart span=1h  limit=0 useother=False partial=False
        sum(csv_fsize_kB) as sum_size,
        sum(duration_sec) as sum_took,
        perc95(duration_sec) as p95_took,
        perc95(single_throughput) as p95_throughput,
        avg(duration_sec) as avg_took,
        avg(single_throughput) as  avg_throughput,
        median(duration_sec) as m_took,
        median(single_throughput) as m_throughput
    by by_clause
| eval node_throughput=sum_size/sum_took
| outputlookup "whole_deployment_month.csv"
"""

def detable():
    with open(lookup_location, 'rb') as csvfile:
        with open('/Applications/Splunk/etc/apps/GoodDataAlerting/lookups/untabled2w.csv', 'wb') as output:
            writer = csv.DictWriter(output, ['_time', 'metric', 'host', 'mode', 'value'], delimiter=',', quotechar="\"")
            writer.writeheader()
            spamreader = csv.DictReader(csvfile, delimiter=',', quotechar="\"")
            for i, row in enumerate(spamreader):
                for k, v in row.items():
                    if k != '_span':
                        try:
                            metric, agg = k.split(': ')
                            host, mode = agg.split('#')
                            if ' ' not in host:
                                try:
                                    v = float(v)
                                except ValueError:
                                    v = None
                                writer.writerow({'_time': row['_time'], 'metric': metric, 'host': host, 'mode': mode, 'value': v})
                        except StandardError:
                            # print 'error'
                            pass

def calc_stats():
    with open('multiple_predict_5.9-20.9.csv', 'rb') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=',', quotechar="\"")
        a = 0
        for i, row in enumerate(spamreader):
            if int(row['_time']) > 1474329600:
                a+=1
        print a

if __name__ == '__main__':
    calc_stats()



m_predicts = """
| inputlookup untabled2w.csv  | where  metric="sum_took" | gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode
| append [ | inputlookup untabled2w.csv  | where  metric="p95_took" | gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| append [ | inputlookup untabled2w.csv  | where  metric="p95_throughput"| gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| append [ | inputlookup untabled2w.csv  | where  metric="avg_took"| gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| append [ | inputlookup untabled2w.csv  | where  metric="avg_throughput"| gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| append [ | inputlookup untabled2w.csv  | where  metric="m_took"| gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| append [ | inputlookup untabled2w.csv  | where  metric="m_throughput"| gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| append [ | inputlookup untabled2w.csv  | where  metric="node_throughput"| gdpredict period=168 algorithm=maxLLP holdback=24 future_timespan=24 value by host, mode]
| table _time, metric, host, mode, value, prediction(value), outliersNum
| outputlookup multiple_predict_5.9-20.9.csv
"""